<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableParticipantAssessments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participant_assessments', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->integer('participant_id');
            $table->uuid('task_uuid');
            $table->uuid('assessment_category_uuid');
            $table->uuid('assessment_element_uuid');
            $table->uuid('assessment_element_scale_uuid');
            $table->integer('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participant_assessments');
    }
}
