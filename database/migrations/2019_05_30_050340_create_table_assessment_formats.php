<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssessmentFormats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_value_elements', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->uuid('assessment_category_uuid');
            $table->string('assesment_element',100);
            $table->integer('element_integrity');
            $table->longText('text_scale_1');
            $table->longText('text_scale_2');
            $table->longText('text_scale_3');
            $table->longText('text_scale_4');
            $table->integer('value_scale_1');
            $table->integer('value_scale_2');
            $table->integer('value_scale_3');
            $table->integer('value_scale_4');
            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_value_elements');
    }
}
