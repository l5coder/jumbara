<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssessmentJudges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessment_judges', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->uuid('group_uuid');
            $table->uuid('task_uuid');
            $table->integer('user_id');
            $table->enum('judge_type',['Kelompok','Individu']);
            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessment_judges');
    }
}
