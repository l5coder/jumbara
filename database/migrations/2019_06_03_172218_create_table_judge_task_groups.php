<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableJudgeTaskGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('judge_task_groups', function (Blueprint $table) {
            $table->uuid('uuid');
            $table->integer('user_id');
            $table->uuid('task_uuid');
            $table->uuid('group_uuid');
            $table->enum('judging_type',['Individu','Kelompok']);
            $table->primary('uuid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('judge_task_groups');
    }
}
