<?php

use Illuminate\Http\Request;


Route::post('auth/login', 'AuthController@login');
Route::get('/information-home','InformationController@showAll');
Route::get('/download-home','DownloadController@showAll');
Route::get('/informasi/{slug}','InformationController@getBySlug');
Route::get('/download-file/{slug}/{fileToDownload}','DownloadController@downloadFile');
Route::get('/participant/statistic','ParticipantController@getParticipantStatistic');
Route::get('/test-count',function(){
    $participantTasks = \App\Models\ParticipantTasksModel::join('participant','participant.id','=','participant_tasks.participant_id')
        ->join('position','position.id','=','participant.position_id')
        ->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
        ->select('participant_tasks.participant_id','participant.full_name','participant.contingent_id','participant.position_id',
            'position.position_name', 'participant_tasks.task_uuid','tasks.task_title')
        ->where('tasks.is_traveling','=',0)
        ->where('participant.contingent_id','=',22)
        ->whereIn('participant.position_id',[17,18,19])
        ->get();
    $data = [];
    foreach ($participantTasks as $participantTask) {
        $data[]=[
            'participantId'=>$participantTask->participant_id,
            'fullName'=>$participantTask->full_name,
            'contingentId'=>$participantTask->contingent_id,
            'positionName'=>$participantTask->position_name,
            'positionId'=>$participantTask->position_id,
            'taskUuid'=>$participantTask->task_uuid,
            'taskTitle'=>$participantTask->task_title
        ];
    }

    return response(['data'=>$data,'count'=>count($data)]);

    //judge_id = 109
    //judge position id 49
    //ane veronica id 316,group uuid ded51855-be55-4ce9-92d1-dbe0a89c6902
    //rico bayu 464, group uuid ded51855-be55-4ce9-92d1-dbe0a89c6902
    //category1 775c66af-503a-4115-afce-10618b4c7417
    //category2 8f9130db-3afc-46c6-a09e-4738c193aae1
    //category3 f7ee75e5-06e4-4867-9626-534c309433ac
    //element1 5c7d641d-6ced-43b8-9d28-75ed1548a812
    //element2 bcf89780-22f5-474f-98fe-b14d389d7d6d
    //element3 dc704145-ceed-4f38-b764-53bef633c392
    //task uuid 28eb04c2-1e1f-4f86-8ec4-8cd72e625cdd
});
Route::post('/test-save',function (){
    $data = [];
    $scores = [];
   $query1 = \App\Models\AssessmentsModel::join('participant','participant.id','=','participant_assessments.participant_id')
       ->select('participant.id','participant.full_name','participant_assessments.assessment_type',
           \Illuminate\Support\Facades\DB::raw('sum(score) as totalScore','participant_assessments.task_uuid'),
           'participant_assessments.judge_id')
       ->where('participant_assessments.task_uuid','=','09cf4930-ab52-4496-af70-4a3c117e8ee4')
       ->where('participant_assessments.judge_id','=',90)
       ->where('participant_assessments.group_uuid','=','b2975c6e-91cf-45a4-abac-07328cecc55c')
       ->where('participant_assessments.assessment_type','=','sikap')
       ->groupBy('participant_assessments.assessment_type','participant.id');
   $assessmentResults =  $query1->get();

   return response(['data'=>$assessmentResults]);
});

//Route::get('nilai','AssessmentsController@getAssessmentsResults');

Route::group(['middleware'=>'jwt.auth'],function (){
    Route::get('auth/user', 'AuthController@user');
    Route::post('user/change-password','AuthController@changePassword');

    Route::get('/user','UserController@showAll');
    Route::post('/user/create','UserController@create');
    Route::get('/user/{id}','UserController@read');
    Route::post('/user/update','UserController@update');
    Route::post('/user/{id}/delete','UserController@delete');
    Route::post('/user/update-profle','UserController@updateProfile');
    Route::put('/user/task-lock/{id}','UserController@taskLockStatus');

    Route::get('/user-level','UserLevelController@showAll');
    Route::post('/user-level/create','UserLevelController@create');
    Route::get('/user-level/{id}','UserLevelController@read');
    Route::post('/user-level/update','UserLevelController@update');
    Route::post('/user-level/{id}/delete','UserLevelController@delete');

    Route::get('/position','PositionController@pagination');
    Route::get('/position/all','PositionController@showAll');
    Route::post('/position/create','PositionController@create');
    Route::get('/position/{id}','PositionController@read');
    Route::post('/position/update','PositionController@update');
    Route::post('/position/{id}/delete','PositionController@delete');

    Route::get('/contingent','ContingentController@pagination');
    Route::get('/contingent/all','ContingentController@showAll');
    Route::post('/contingent/create','ContingentController@create');
    Route::get('/contingent/{id}','ContingentController@read');
    Route::post('/contingent/update','ContingentController@update');
    Route::post('/contingent/{id}/delete','ContingentController@delete');

    Route::get('/participant','ParticipantController@pagination');
    Route::get('/participant-contingent/{contingentId}','ParticipantController@paginationByContingent');
    Route::get('/participant/all','ParticipantController@showAll');
    Route::post('/participant/create','ParticipantController@create');
    Route::get('/participant/{id}','ParticipantController@read');
    Route::post('/participant/update','ParticipantController@update');
    Route::post('/participant/{id}/delete','ParticipantController@delete');
    Route::post('/participant/upload-document','ParticipantController@uploadDocument');
    Route::post('/participant/delete-document','ParticipantController@deleteDocument');
    Route::get('/participant/task/{contingentId}','ParticipantController@getParticipantForTask');
    Route::get('/participant/by-task/{taskUuid}','ParticipantController@getParticipantByTask');
    Route::get('/participant-judge','ParticipantController@getParticipantForJudge');


    Route::get('/information','InformationController@showAll');
    Route::post('/information/create','InformationController@create');
    Route::get('/information/{id}','InformationController@read');
    Route::post('/information/update','InformationController@update');
    Route::post('/information/{id}/delete','InformationController@delete');
    Route::post('/information/upload-image','InformationController@uploadImage');

    Route::get('/download','DownloadController@showAll');
    Route::post('/download/create','DownloadController@create');
    Route::get('/download/{id}','DownloadController@read');
    Route::post('/download/update','DownloadController@update');
    Route::post('/download/{id}/delete','DownloadController@delete');
    Route::get('/download/{slug}','DownloadController@getBySlug');
    Route::post('/download/upload-download-document','DownloadController@uploadDownloadDocument');


    Route::get('/task-category','TaskCategoriesController@showAll');
    Route::post('/task-category','TaskCategoriesController@create');
    Route::get('/task-category/{uuid}','TaskCategoriesController@read');
    Route::put('/task-category/{uuid}','TaskCategoriesController@update');
    Route::delete('/task-category/{uuid}','TaskCategoriesController@delete');

    Route::get('/task','TasksController@showAll');
    Route::post('/task','TasksController@create');
    Route::get('/task/{uuid}','TasksController@read');
    Route::put('/task/{uuid}','TasksController@update');
    Route::delete('/task/{uuid}','TasksController@delete');
    Route::get('/task-non-masal','TasksController@getValuationNonMasal');
    Route::get('/task-valuation','TasksController@getValuationTask');

    Route::get('/schedule','SchedulesController@showAll');
    Route::post('/schedule','SchedulesController@create');
    Route::get('/schedule/{uuid}','SchedulesController@read');
    Route::put('/schedule/{uuid}','SchedulesController@update');
    Route::delete('/schedule/{uuid}','SchedulesController@delete');
    Route::get('/schedule-valuation/all','SchedulesController@getScheduleValuationAll');
    Route::get('/schedule-valuation/{uuid}','SchedulesController@getScheduleValuationFiltered');


    Route::post('/participant-task','ParticipantTasksController@create');
    Route::get('/participant-task/{contingentId}','ParticipantTasksController@getParticipantTaskByContingent');
    Route::delete('/participant-task/participant/{participantId}','ParticipantTasksController@deleteByParticipant');
    Route::get('/participant-task-get-export/{taskUuid}','ParticipantTasksController@getAllParticipantTravelingTask');

    Route::get('/forpen','AssessmentFormatController@showAll');
    Route::post('/forpen','AssessmentFormatController@create');
    Route::get('/forpen/{uuid}','AssessmentFormatController@read');
    Route::put('/forpen/{uuid}','AssessmentFormatController@update');
    Route::delete('/forpen/{uuid}','AssessmentFormatController@delete');
    Route::get('/forpen/type/{type}/{isTraveling}/{taskUuid}/{groupUuid}/{judgeId}','AssessmentFormatController@getByTravelingType');

    Route::get('/group','GroupsController@showAll');
    Route::post('/group','GroupsController@create');
    Route::get('/group/{uuid}','GroupsController@read');
    Route::put('/group/{uuid}','GroupsController@update');
    Route::delete('/group/{uuid}','GroupsController@delete');
    Route::get('/group-task/{taskUuid}','GroupsController@getByTask');
    Route::get('/participant/group/{groupUuid}','GroupsController@getByGroup');

    Route::get('/penilai','JudgeTaskGroupsController@showAll');
    Route::post('/penilai','JudgeTaskGroupsController@create');
    Route::get('/penilai/{uuid}','JudgeTaskGroupsController@read');
    Route::put('/penilai/{uuid}','JudgeTaskGroupsController@update');
    Route::delete('/penilai/{uuid}','JudgeTaskGroupsController@delete');
    Route::get('/penilai-task/{id}','JudgeTaskGroupsController@getJudgeTask');
    Route::get('/penilai/juri/{judgeId}/task/{taskUuid}','JudgeTaskGroupsController@getByJudgeTask');

    Route::post('/menilai','AssessmentsController@create');
    Route::get('/menilai/{judgeId}/task/{taskUuid}/group/{groupUuid}','AssessmentController@getNilaiPerJudgeTaskGroup');

    Route::get('/nilai-non-traveling/{judgeId}','AssessmentsController@getAssessmentResultNonTravelingByJudge');
    Route::get('/nilai-traveling/{judgeId}','AssessmentsController@getAssessmentResultTravelingByJudge');

    Route::post('/nilai-pengetahuan','KnowledgeAssessmentsController@create');

    Route::get('/nilai/pmr/traveling','AssessmentsController@getAssessmentResultTravelingPmr');
    Route::get('/nilai/pmr/non-traveling','AssessmentsController@getAssessmentResultNonTravelingPmr');
    Route::get('/nilai/fasilitator/traveling','AssessmentsController@getAssessmentResultTravelingFasilitator');
    Route::get('/nilai-pmr-kontingen/{isTraveling}/{contingentId}','AssessmentsController@getPmrAssessmentResultByContingent');
    Route::get('/nilai-fasilitator-kontingen/{isTraveling}/{contingentId}','AssessmentsController@getFasilitatorAssessmentResultsByContingent');

});

Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');

});