<?php

namespace App\Http\Controllers;

use App\Services\AssessmentFormatService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AssessmentFormatController extends Controller
{
    protected $assessmentFormatService;

    public function __construct(AssessmentFormatService $assessmentFormatService)
    {
        $this->assessmentFormatService  = $assessmentFormatService;
    }

    public function create(){
        $result = $this->assessmentFormatService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->assessmentFormatService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->assessmentFormatService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->assessmentFormatService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->assessmentFormatService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getByTravelingType($type,$isTraveling,$taskUuid,$groupUuid,$judgeId){
        $result = $this->assessmentFormatService->getByTravelingType($isTraveling,$type,$taskUuid,$groupUuid,$judgeId);

        return $this->getJsonResponse($result);
    }
}
