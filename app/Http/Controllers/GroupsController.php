<?php

namespace App\Http\Controllers;

use App\Services\GroupsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class GroupsController extends Controller
{
    protected $groupsService;

    public function __construct(GroupsService $groupsService)
    {
        $this->groupsService = $groupsService;
    }

    public function create(){
        $result = $this->groupsService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->groupsService->showAll();

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->groupsService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getByTask($taskUuid){
        $result = $this->groupsService->getByTask($taskUuid);

        return $this->getJsonResponse($result);
    }

    public function getByGroup($groupUuid){
        $result = $this->groupsService->getByGroup($groupUuid);

        return $this->getJsonResponse($result);
    }
}
