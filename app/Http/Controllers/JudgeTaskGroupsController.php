<?php

namespace App\Http\Controllers;

use App\Services\JudgeTaskGroupsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class JudgeTaskGroupsController extends Controller
{
    protected  $judgeTaskGroupsService;

    public function __construct(JudgeTaskGroupsService $judgeTaskGroupsService)
    {
        $this->judgeTaskGroupsService = $judgeTaskGroupsService;
    }

    public function create(){
        $result = $this->judgeTaskGroupsService->create(Input::all());

        return $this->getJsonResponse($result);
    }

    public function read($uuid){
        $result = $this->judgeTaskGroupsService->read($uuid);

        return $this->getJsonResponse($result);
    }

    public function showAll(){
        $result = $this->judgeTaskGroupsService->showAll();

        return $this->getJsonResponse($result);
    }

    public function update($uuid){
        $result = $this->judgeTaskGroupsService->update(Input::all(),$uuid);

        return $this->getJsonResponse($result);
    }

    public function delete($uuid){
        $result = $this->judgeTaskGroupsService->delete($uuid);

        return $this->getJsonResponse($result);
    }

    public function getJudgeTask($judgeId){
        $result = $this->judgeTaskGroupsService->getJudgeTask($judgeId);

        return $this->getJsonResponse($result);
    }

    public function getByJudgeTask($judgeId,$taskUuid){
        $result = $this->judgeTaskGroupsService->getByJudgeTask($judgeId,$taskUuid);

        return $this->getJsonResponse($result);
    }
}
