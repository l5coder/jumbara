<?php

namespace App\Http\Controllers;

use App\Imports\KnowledgeAssessmentsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class KnowledgeAssessmentsController extends Controller
{
    public function create(Request $request){
        $file = $request->file('file');
        $name = date('Y-m-d H:i:s').''.$file->getClientOriginalName();
        $file->storeAs('public/nilai',$name);
        try{
            if(Excel::import(new KnowledgeAssessmentsImport,storage_path('app/public/nilai/'.$name)));
        }catch (\Exception $exception){
            return response(['message'=>$exception->getMessage()],500);
        }
    }
}
