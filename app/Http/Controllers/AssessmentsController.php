<?php

namespace App\Http\Controllers;

use App\Services\AssessmentsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AssessmentsController extends Controller
{
    protected $assessmentsService;

    public function __construct(AssessmentsService $assessmentsService)
    {
        $this->assessmentsService = $assessmentsService;
    }

    public function create(){
        $result = $this->assessmentsService->create(Input::all());

        return $this->getJsonResponse($result);

//        return response(['res'=>$result->getErrorMessages()]);

//        $input = Input::all();
//        $participantAssessments = $input['participantAssessments'];
//        foreach ($participantAssessments as $participantAssessment){
//            $scores = $participantAssessment['scores'];
//            foreach ($scores as $score) {
//                $bobot = intval($score['value'])*25;
//                $valueScore = (intval($score['integrity'])/100)*$bobot;
//                $param[] = [
//                    'judgeId'=>$input['judgeId'],
//                    'participantId'=>$participantAssessment['participantId'],
//                    'taskUuid'=>$input['taskUuid'],
//                    'groupUuid'=>$input['groupUuid'],
//                    'type'=>$input['type'],
//                    'elementUuid'=>$score['elementUuid'],
//                    'assessmentType'=>$score['type'],
//                    'scaleValue'=>$score['value'],
//                    'bobot'=>$bobot,
//                    'score'=>$valueScore
//                ];
//            }
//        }
//
//        return response(['tes'=>$param]);
    }

    public function getAssessmentResultNonTravelingByJudge($judgeId){
        $result = $this->assessmentsService->getAssessmentResultsByJudgeNonTraveling($judgeId);

        return $this->getJsonResponse($result);
//        return response(['tes'=>Input::get('judgeId')]);
    }

    public function getAssessmentResultTravelingByJudge($judgeId){
        $result = $this->assessmentsService->getAssessmentResultsByJudgeTraveling($judgeId);

        return $this->getJsonResponse($result);
    }

    public function getAssessmentResultTravelingPmr(){
        $result = $this->assessmentsService->getAssessmentResultTravelingPmr();

        return $this->getJsonResponse($result);
    }

    public function getAssessmentResultNonTravelingPmr(){
        $result = $this->assessmentsService->getAssessmentResultNonTravelingPmr();

        return $this->getJsonResponse($result);
    }

        public function getAssessmentResultTravelingFasilitator(){
        $result = $this->assessmentsService->getAssessmentResultFasilitatorTraveling();

        return $this->getJsonResponse($result);
    }

    public function getPmrAssessmentResultByContingent($isTraveling,$contingentId){
        $result = $this->assessmentsService->getPmrAssessmentResultByContingent($contingentId,$isTraveling);

        return $this->getJsonResponse($result);
//        return response(['tes'=>'tes'],200);
    }

    public function getFasilitatorAssessmentResultsByContingent($isTraveling,$contingentId){
        $result = $this->assessmentsService->getFasilitatorAssessmentResultByContingent($contingentId,$isTraveling);

        return $this->getJsonResponse($result);
    }
}
