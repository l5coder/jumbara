<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryAppProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('App\\Repositories\\Contracts\\IUserRepository', 'App\\Repositories\\Actions\UserRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IUserLevelRepository', 'App\\Repositories\\Actions\UserLevelRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IPositionRepository', 'App\\Repositories\\Actions\PositionRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IContingentRepository', 'App\\Repositories\\Actions\ContingentRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IParticipantRepository', 'App\\Repositories\\Actions\ParticipantRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IInformationRepository', 'App\\Repositories\\Actions\InformationRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IDownloadRepository', 'App\\Repositories\\Actions\DownloadRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ITaskCategoriesRepository', 'App\\Repositories\\Actions\TaskCategoriesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ITasksRepository', 'App\\Repositories\\Actions\TasksRepository');
        $this->app->bind('App\\Repositories\\Contracts\\ISchedulesRepository', 'App\\Repositories\\Actions\SchedulesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IParticipantTasksRepository', 'App\\Repositories\\Actions\ParticipantTasksRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IAssessmentCategoriesRepository',
            'App\\Repositories\\Actions\AssessmentCategoriesRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IAssessmentElementsRepository', 'App\\Repositories\\Actions\AssessmentElementsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IAssessmentElementScalesRepository',
            'App\\Repositories\\Actions\AssessmentElementScalesRepository');

        $this->app->bind('App\\Repositories\\Contracts\\ITaskGroupsRepository', 'App\\Repositories\\Actions\TaskGroupsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IParticipantGroupsRepository', 'App\\Repositories\\Actions\ParticipantGroupsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IJudgeTaskGroupsRepository', 'App\\Repositories\\Actions\JudgeTaskGroupsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IAssessmentsRepository', 'App\\Repositories\\Actions\AssessmentsRepository');
        $this->app->bind('App\\Repositories\\Contracts\\IKnowledgeAssessmentsRepository', 'App\\Repositories\\Actions\KnowledgeAssessmentsRepository');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
