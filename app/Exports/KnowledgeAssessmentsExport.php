<?php

namespace App\Exports;


use App\Models\KnowledgeAssessmentsModel;
use Maatwebsite\Excel\Concerns\FromCollection;

class KnowledgeAssessmentsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return KnowledgeAssessmentsModel::all();
    }
}
