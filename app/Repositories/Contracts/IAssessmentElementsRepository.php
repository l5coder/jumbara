<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:13
 */

namespace App\Repositories\Contracts;


interface IAssessmentElementsRepository extends IBaseRepository
{
    public function isAssessmentElementExist($assessmentCategoryUuid,$element,$uuid = null);

    public function readByAssessmentCategory($assessmentCategoryUuid);

    public function massDelete($assessmentCategoryUuid,$uuids);

    public function deleteByAssessmentCategory($assessmentCategoryUuid);

}