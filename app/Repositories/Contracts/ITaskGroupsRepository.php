<?php

namespace App\Repositories\Contracts;


interface ITaskGroupsRepository extends IBaseRepository
{
    public function getByTask($taskUuid);

    public function getCountGroup($taskUuid);
}