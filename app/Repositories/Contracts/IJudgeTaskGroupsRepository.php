<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-04
 * Time: 00:26
 */

namespace App\Repositories\Contracts;


interface IJudgeTaskGroupsRepository extends IBaseRepository
{
    public function isJudgeExistOnUserGroup($userId,$groupUuid,$taskUuid);

    public function getJudgeTask($judgeId);

    public function getByJudgeTask($judgeId,$taskUuid);

    public function setLock($groupUuid,$taskUuid);

    public function getGroupsByJudgeGroupsNonTraveling($judgeId);

    public function getGroupByJudgeGroupsTraveling($judgeId);
}