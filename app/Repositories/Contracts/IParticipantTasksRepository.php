<?php

namespace App\Repositories\Contracts;


interface IParticipantTasksRepository extends IBaseRepository
{
    public function getParticipantTasksByContingent($contingentId);

    public function deleteTask($participantId,$taskUuid);

    public function isParticipantTaskExist($participantId,$taskUuid,$uuid = null);

    public function isParticipantHasTask($participantId);

    public function deleteByParticipant($participantId);

    public function getAllParticipantTravelingTask($taskUuid);

    public function getTaskTravelingParticipantPmr();

    public function getTaskNonTravelingParticipantPmr();

    public function getTaskTravelingFasilitator();

    public function getPmrParticipantTaskByContingent($contingentId,$isTraveling);

    public function getFasilitatorParticipantTaskByContingent($contingentId,$isTraveling);
}