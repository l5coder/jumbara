<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-02
 * Time: 02:25
 */

namespace App\Repositories\Contracts;


interface IParticipantGroupsRepository extends IBaseRepository
{
    public function isParticipantExistInOtherGroup($participantId,$taskUuid,$groupUuid);

    public function deleteByGroup($groupUuid);

    public function getByGroup($groupUuid);

    public function getByGroupByTask($groupUuid,$taskUuid);

    public function isParticipantGroupExist($groupUuid);

    public function getParticipantByGroup($groupUuid);
}