<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:47
 */

namespace App\Repositories\Contracts;


interface IAssessmentElementScalesRepository extends IBaseRepository
{
    public function isElementScaleExist($elementUuid,$textScale,$valueScale,$uuid = null);

    public function getByAssessmentElement($elementUuid);

    public function massDelete($elementUuid,$uuids);

    public function deleteByElementUuid($elementUuid);
}