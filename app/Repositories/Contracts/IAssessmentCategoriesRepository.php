<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:04
 */

namespace App\Repositories\Contracts;


interface IAssessmentCategoriesRepository extends IBaseRepository
{
    public function isAssessmentCategoryExist($assessmentCategory,$taskUuid,$uuid = null);

    public function getByTravelingType($isTraveling,$assessmentType,$taskUuid);
}