<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-13
 * Time: 05:07
 */

namespace App\Repositories\Contracts;


interface IAssessmentsRepository extends IBaseRepository
{
    public function getByContingent($contingentId);

    public function getByTask($taskUuid);

    public function getScore($groupUuid);

    public function getByJudgeTaskGroupTraveling($judgeId);

    public function getAssessmentResultByJudgeNonTraveling($taskUuid,$groupUuid,$participantId,$judgeId);

    public function getAssessmentResultByJudgeTraveling($taskUuid,$groupUuid,$participantId,$judgeId);

    public function isAssessmentResultByJudgeTaskGroupExist($taskUuid,$groupUuid,$participantId,$judgeId);

    public function isAssessmentExist($assessmentType,$taskUuid,$groupUuid,$judgeId);

    public function isAssessmentByParticipantTaskExist($participantId,$taskUuid);

    public function getAssessmentResultByParticipantTaskTraveling($participantId,$taskUuid);

    public function getAssessmentResultByParticipantTaskNonTraveling($participantId,$taskUuid);

    public function getAssessmentResultFasilitatorTraveling($participantId,$taskUuid);
}