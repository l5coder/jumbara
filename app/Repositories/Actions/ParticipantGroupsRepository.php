<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-02
 * Time: 02:30
 */

namespace App\Repositories\Actions;


use App\Models\ParticipantGroupsModel;
use App\Repositories\Contracts\IParticipantGroupsRepository;

class ParticipantGroupsRepository implements IParticipantGroupsRepository
{

    public function create($input)
    {
        $participantGroup = new ParticipantGroupsModel();
        $participantGroup->participant_id = $input['participantId'];
        $participantGroup->task_uuid = $input['taskUuid'];
        $participantGroup->group_uuid = $input['groupUuid'];

        return $participantGroup->save();
    }

    public function update($input)
    {
        $participantGroup = ParticipantGroupsModel::find($input['uuid']);
        $participantGroup->participant_id = $input['participantId'];
        $participantGroup->task_uuid = $input['taskUuid'];

        return $participantGroup->save();
    }

    public function delete($id)
    {
        return ParticipantGroupsModel::uuid($id)->delete();
    }

    public function read($id)
    {
        return ParticipantGroupsModel::find($id);
    }

    public function showAll()
    {
        // TODO: Implement showAll() method.
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isParticipantExistInOtherGroup($participantId, $taskUuid, $groupUuid)
    {
        $result = ParticipantGroupsModel::where(function ($q)use($participantId,$taskUuid){
            $q->where('participant_id','=',$participantId)
                ->where('task_uuid','=',$taskUuid);
        })->where('group_uuid','<>',$groupUuid)->count();

        return ($result>0);
    }

    public function deleteByGroup($groupUuid)
    {
        return ParticipantGroupsModel::where('group_uuid','=',$groupUuid)->delete();
    }

    public function getByGroup($groupUuid)
    {
        $participants = ParticipantGroupsModel::join('participant','participant.id','=','participant_groups.participant_id')
            ->join('task_groups','task_groups.uuid','=','participant_groups.group_uuid')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->select('participant_groups.uuid','participant_groups.participant_id','participant.full_name','participant.photograph','position.position_name','contingent.contingent_name')
            ->where('participant_groups.group_uuid','=',$groupUuid)
            ->get();
        $data = [];

        foreach ($participants as $participant){
            $data[]=[
                'uuid'=>$participant->uuid,
                'participantId'=>$participant->participant_id,
                'fullName'=>$participant->full_name,
                'contingentName'=>$participant->contingent_name,
                'positionName'=>$participant->position_name,
                'photograph'=>$participant->photograph
            ];
        }

        return $data;
    }

    public function getByGroupByTask($groupUuid, $taskUuid)
    {
        $participants = ParticipantGroupsModel::join('participant','participant.id','=','participant_groups.participant_id')
            ->join('task_groups','task_groups.uuid','=','participant_groups.group_uuid')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->select('participant_groups.uuid','participant_groups.participant_id','participant.full_name','participant.photograph','position.position_name','contingent.contingent_name')
            ->where('participant_groups.group_uuid','=',$groupUuid)
            ->where('participant_groups.task_uuid','=',$taskUuid)
            ->get();
        $data = [];

        foreach ($participants as $participant){
            $data[]=[
                'groupUuid'=>$participant->uuid,
                'participantId'=>$participant->participant_id,
                'fullName'=>$participant->full_name,
                'contingentName'=>$participant->contingent_name,
                'positionName'=>$participant->position_name,
                'photograph'=>$participant->photograph
            ];
        }

        return $data;
    }


    public function isParticipantGroupExist($groupUuid)
    {
        $result = ParticipantGroupsModel::select('group_uuid')->where('group_uuid','=',$groupUuid)->distinct('group_uuid')->count();

        return ($result>0);
    }

    public function getParticipantByGroup($groupUuid)
    {
        $participantGroups = ParticipantGroupsModel::join('participant','participant.id','=','participant_groups.participant_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->select('participant_groups.participant_id','participant.full_name','contingent.contingent_name')
            ->where('group_uuid','=',$groupUuid)->get();
        $data = [];
        foreach ($participantGroups as $participantGroup){
            $data[]=[
                'participantId'=>$participantGroup->participant_id,
                'fullName'=>$participantGroup->full_name,
                'contingentName'=>$participantGroup->contingent_name
            ];
        }

        return $data;
    }
}