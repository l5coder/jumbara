<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-13
 * Time: 05:07
 */

namespace App\Repositories\Actions;


use App\Models\AssessmentsModel;
use App\Models\JudgeTaskGroupsModel;
use App\Models\KnowledgeAssessmentsModel;
use App\Models\ParticipantModel;
use App\Repositories\Contracts\IAssessmentsRepository;
use Illuminate\Support\Facades\DB;

class AssessmentsRepository implements IAssessmentsRepository
{

    public function create($input)
    {
        $assessment = new AssessmentsModel();
        $assessment->judge_id = $input['judgeId'];
        $assessment->participant_id = $input['participantId'];
        $assessment->task_uuid = $input['taskUuid'];
        $assessment->group_uuid = $input['groupUuid'];
        $assessment->type = $input['type'];
        $assessment->assessment_element_uuid = $input['elementUuid'];
        $assessment->assessment_category_uuid = $input['assessmentCategoryUuid'];
        $assessment->assessment_type = $input['assessmentType'];
        $assessment->scale_value = $input['scaleValue'];
        $assessment->bobot = $input['bobot'];
        $assessment->score = $input['score'];
        $assessment->judge_position_id = $input['judgePositionId'];
        $assessment->save();

        return $assessment->uuid;
    }

    public function update($input)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function read($id)
    {
        // TODO: Implement read() method.
    }

    public function showAll()
    {
        // TODO: Implement showAll() method.
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function getByContingent($contingentId)
    {
        $assessments = AssessmentsModel::join('participant','participant.id','=','participant_assessments.participant_id')
            ->select('participant_assessments.*','participant.contingent_id','participant.full_name')
            ->where('participant.contingent_id','=',$contingentId)
            ->get();
        $data = [];
        foreach ($assessments as $assessment){
            $data[]=[
                'participantId'=>$assessments->participant_id,
                'fullName'=>$assessments->full_name,
                'total'=>$assessments->total
            ];
        }

        return $data;
    }

    public function getByTask($taskUuid)
    {
        // TODO: Implement getByTask() method.
    }

    public function getScore($groupUuid)
    {
        $data = [];
        $types = AssessmentsModel::select('assessment_type')->distinct('assessment_type')->get();
        foreach ($types as $type){
            $assessments = AssessmentsModel::where('groupUuid','=',$groupUuid)
                ->join('participant','participant.id','=','participant_assessments.participant_id')
                ->select('participant_assessments.*','participant.full_name')
                ->where('participant_assessments.group_uuid','=',$groupUuid)
                ->where('participant_assessments.assessment_type','=',$type)
            ->get();
            foreach($assessments as $assessment){

            }
        }


    }

    public function getByJudgeTaskGroupTraveling($judgeId)
    {
//        $data = [];
//       $query = JudgeTaskGroupsModel::query();
//       $query->join('participant','participant.','=','judge_task_groups.user_id')
//           ->join('position','position.id','=','participant.position_id')
//           ->join('participant_groups','participant_groups.group_uuid','=','judge_task_groups.group_uuid')
//           ->join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
//           ->select('participant_groups.participant_id','judge_task_groups.user_id','judge_task_groups.task_uuid','judge_task_groups.group_uuid',
//               'position.id as position_id','tasks.is_traveling')
//           ->where('judge_task_groups.user_id','=',$judgeId)
//           ->where('tasks.is_traveling','=',1);
//       $coreDatas = $query->get();
//       $data2 = [];
//       $scoreSikapData =[];
//
//       foreach ($coreDatas as $coreData){
//           $scoreSikap = 0;
//           $scoreSikapQueries = AssessmentsModel::join('participant','participant.id','=','participant_assessments.participant_id')
//               ->join('position','position.id','=','participant.position_id')
//               ->join('contingent','contingent.id','=','participant.contingent_id')
//               ->select('score','')
//               ->where('judge_id','=',$judgeId)
//               ->where('participant_id','=',$coreData['participant_id'])
//               ->where('group_uuid','=',$coreData['group_uuid'])
//               ->where('task_uuid','=',$coreData['task_uuid'])
//               ->where('assessment_type','=','sikap')
//               ->get();
//
//       }
//
//       return $scoreSikapQueries;
    }

    public function getAssessmentResultByJudgeNonTraveling($taskUuid, $groupUuid, $participantId,$judgeId)
    {
        $scoreSikap = 0;
        $scoreKetrampilan = 0;
        $scorePengetahuan = 0;

        $sikapData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('group_uuid','=',$groupUuid)->where('participant_id','=',$participantId)
            ->where('assessment_type','=','sikap')
            ->where('judge_id','=',$judgeId)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        $ketrampilanData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('participant_id','=',$participantId)
            ->where('assessment_type','=','ketrampilan')
            ->where('judge_id','=',$judgeId)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        $pengetahuanData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('group_uuid','=',$groupUuid)->where('participant_id','=',$participantId)
            ->where('assessment_type','=','pengetahuan')
            ->where('judge_id','=',$judgeId)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        if(count($pengetahuanData)>0){
            foreach($pengetahuanData as $pengetahuanData){
                $scorePengetahuan = $scorePengetahuan+floatval($pengetahuanData['score']);
            }
        }else{
            $scorePengetahuan = KnowledgeAssessmentsModel::where('task_uuid','=',$taskUuid)->where('participant_id','=',$participantId)->value('score');
        }

        foreach ($sikapData as $sikapData){
            $scoreSikap = $scoreSikap+floatval($sikapData['score']);
        }

        foreach ($ketrampilanData as $ketrampilanData){
            $scoreKetrampilan = $scoreKetrampilan+floatval($ketrampilanData['score']);
        }


        return [
            'scoreSikap'=>$scoreSikap,
            'scoreKetrampilan'=>$scoreKetrampilan,
            'scorePengetahuan'=>$scorePengetahuan,
            'total'=>round($scoreSikap+$scoreKetrampilan+$scorePengetahuan,1)
        ];
    }

    public function getAssessmentResultByJudgeTraveling($taskUuid, $groupUuid, $participantId, $judgeId)
    {
        $scoreSikap = 0;
        $scoreKetrampilan = 0;
        $scorePengetahuan = 0;

        if($participantId == 1613 || $participantId == 1668 || $participantId == 1472){
            $sikapData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('group_uuid','=',$groupUuid)->where('participant_id','=',$participantId)
                ->where('assessment_type','=','sikap')
                ->where('judge_id','=',$judgeId)
                ->select('score')
                ->get();
        }else{
            $sikapData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('group_uuid','=',$groupUuid)->where('participant_id','=',$participantId)
                ->where('assessment_type','=','sikap')
                ->where('judge_id','=',$judgeId)
                ->select('score')
                ->groupBy('assessment_element_uuid')
                ->get();
        }

        $ketrampilanData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('participant_id','=',$participantId)
            ->where('assessment_type','=','ketrampilan')
            ->where('judge_id','=',$judgeId)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        $pengetahuanData = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('group_uuid','=',$groupUuid)->where('participant_id','=',$participantId)
            ->where('assessment_type','=','pengetahuan')
            ->where('judge_id','=',$judgeId)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        if(count($pengetahuanData)>0){
            foreach ($pengetahuanData as $pengetahuanData){
                $scorePengetahuan = $scorePengetahuan + $pengetahuanData['score'];
            }
        }else{
            $scorePengetahuan = KnowledgeAssessmentsModel::where('task_uuid','=',$taskUuid)->where('participant_id','=',$participantId)->value('score');
        }

        foreach ($sikapData as $sikapData){
            $scoreSikap = $scoreSikap+round(floatval($sikapData['score']),1);
        }

        foreach ($ketrampilanData as $ketrampilanData){
            $scoreKetrampilan = $scoreKetrampilan+floatval($ketrampilanData['score']);
        }


        return [
            'scoreSikap'=>$scoreSikap,
            'scoreKetrampilan'=>$scoreKetrampilan,
            'scorePengetahuan'=>$scorePengetahuan,
            'total'=>round($scoreSikap+$scoreKetrampilan+$scorePengetahuan,1)
        ];
    }


    public function isAssessmentResultByJudgeTaskGroupExist($taskUuid, $groupUuid, $participantId,$judgeId)
    {
        $result = AssessmentsModel::where('task_uuid','=',$taskUuid)->where('group_uuid','=',$groupUuid)->where('participant_id','=',$participantId)
            ->where('judge_id','=',$judgeId)
            ->select(DB::raw('sum(score) as score_sikap'))
            ->count();

        return ($result>0);
    }


    public function isAssessmentExist($assessmentType, $taskUuid, $groupUuid,$judgeId)
    {
        $result = AssessmentsModel::where('assessment_type','=',$assessmentType)
            ->where('judge_id','=',$judgeId)
            ->where('task_uuid','=',$taskUuid)
            ->where('group_uuid','=',$groupUuid)
            ->count();

        return ($result > 0);
    }

    public function isAssessmentByParticipantTaskExist($participantId, $taskUuid)
    {
        $result = AssessmentsModel::where('participant_id','=',$participantId)
            ->where('task_uuid','=',$taskUuid)
            ->count();

        return ($result>0);
    }


    public function getScoreSikapFasilitatorPmrTraveling($participantId,$taskUuid){
        $scoreSikapFasilitator = 0;
        $sikapDataFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','sikap')
            ->where('judge_position_id','=',35)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        foreach ($sikapDataFasilitators as $sikapDataFasilitator){
            $scoreSikapFasilitator = $scoreSikapFasilitator + floatval($sikapDataFasilitator['score']);
        }

        return $scoreSikapFasilitator;
    }

    public function getScoreSikapPendamping($participantId,$taskUuid){
        $scoreSikapPendampingFasilitator = 0;
        $sikapDataPendampingFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','sikap')
            ->where('judge_position_id','=',49)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        foreach ($sikapDataPendampingFasilitators as $dataPendampingFasilitator){
            $scoreSikapPendampingFasilitator = $scoreSikapPendampingFasilitator + floatval($dataPendampingFasilitator['score']);
        }

        return $scoreSikapPendampingFasilitator;
    }

    public function getScoreKetrampilanFasilitator($participantId,$taskUuid){
        $scoreKetrampilanFasilitator = 0;
        $ketrampilanDataFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','ketrampilan')
            ->where('judge_position_id','=',35)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        foreach ($ketrampilanDataFasilitators as $ketrampilanDataFasilitator){
            $scoreKetrampilanFasilitator = $scoreKetrampilanFasilitator+floatval($ketrampilanDataFasilitator['score']);
        }

        return $scoreKetrampilanFasilitator;
    }

    public function getScoreKetrampilanPendamping($participantId,$taskUuid){
        $scoreKetrampilanPendampingFasilitator = 0;
        $ketrampilanDataPendampingFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','ketrampilan')
            ->where('judge_position_id','=',49)
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        foreach($ketrampilanDataPendampingFasilitators as $ketrampilanDataPendampingFasilitator){
            $scoreKetrampilanPendampingFasilitator = $scoreKetrampilanPendampingFasilitator+floatval($ketrampilanDataPendampingFasilitator['score']);
        }

        return $scoreKetrampilanPendampingFasilitator;
    }



    public function getAssessmentResultByParticipantTaskTraveling($participantId, $taskUuid)
    {

        $scoreKetrampilanFasilitator = 0;
        $scoreKetrampilanPendampingFasilitator = 0;

        //nilai sikap fasilitator dan pendamping
        $scoreSikapFasilitator = $this->getScoreSikapFasilitatorPmrTraveling($participantId,$taskUuid);
        $scoreSikapPendampingFasilitator = $this->getScoreSikapPendamping($participantId,$taskUuid);
        $totalScoreSikap = round(($scoreSikapFasilitator*0.2)+($scoreSikapPendampingFasilitator*0.8),1);
        //end nilai sikap

        //nilai ketrampilan fasilitator dan pendamping
        $scoreKetrampilanFasilitator = $this->getScoreKetrampilanFasilitator($participantId,$taskUuid);
        $scoreKetrampilanPendampingFasilitator = $this->getScoreKetrampilanPendamping($participantId,$taskUuid);
        $totalScoreKetrampilan = round(($scoreKetrampilanFasilitator*0.2)+($scoreKetrampilanPendampingFasilitator*0.8),1);
        //end nilai ketrampilan

        $scorePengetahuan = 0;
        $scorePengetahuan = KnowledgeAssessmentsModel::where('task_uuid','=',$taskUuid)->where('participant_id','=',$participantId)->value('score');


        return [
            'scoreSikap'=>$totalScoreSikap,
            'scoreKetrampilan'=>$totalScoreKetrampilan,
            'scorePengetahuan'=>$scorePengetahuan,
            'total'=>round($totalScoreSikap+$totalScoreKetrampilan+$scorePengetahuan,1)
        ];
    }

    public function getScoreSikap($participantId,$taskUuid){
        $scoreSikapPendampingFasilitator = 0;
        $sikapDataPendampingFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','sikap')
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        foreach ($sikapDataPendampingFasilitators as $dataPendampingFasilitator){
            $scoreSikapPendampingFasilitator = $scoreSikapPendampingFasilitator + floatval($dataPendampingFasilitator['score']);
        }

        return $scoreSikapPendampingFasilitator;
    }

    public function getScoreKetrampilan($participantId,$taskUuid){
        $scoreKetrampilanPendampingFasilitator = 0;
        $ketrampilanDataPendampingFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','ketrampilan')
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        foreach($ketrampilanDataPendampingFasilitators as $ketrampilanDataPendampingFasilitator){
            $scoreKetrampilanPendampingFasilitator = $scoreKetrampilanPendampingFasilitator+floatval($ketrampilanDataPendampingFasilitator['score']);
        }

        return $scoreKetrampilanPendampingFasilitator;
    }

    public function getScorePengetahuan($participantId,$taskUuid){
        $scorePengetahuan = 0;
        $pengetahuan = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','pengetahuan')
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        foreach($pengetahuan as $pengetahuan){
            $scorePengetahuan = $scorePengetahuan+floatval($pengetahuan['score']);
        }

        return $scorePengetahuan;
    }

    public function getAssessmentResultByParticipantTaskNonTraveling($participantId,$taskUuid){
        $scoreSikap = $this->getScoreSikap($participantId,$taskUuid);
        $scoreKetrampilan = $this->getScoreKetrampilan($participantId,$taskUuid);
        $scorePengetahuan = $this->getScorePengetahuan($participantId,$taskUuid);

        return [
            'scoreSikap'=>$scoreSikap,
            'scoreKetrampilan'=>$scoreKetrampilan,
            'scorePengetahuan'=>$scorePengetahuan,
            'total'=>round($scoreSikap+$scoreKetrampilan+$scorePengetahuan,1)
        ];
    }

    public function getFasilitatorScoreSikap($participantId,$taskUuid){
        $scoreSikapPendampingFasilitator = 0;
        $sikapDataPendampingFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','sikap')
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();

        foreach ($sikapDataPendampingFasilitators as $dataPendampingFasilitator){
            $scoreSikapPendampingFasilitator = $scoreSikapPendampingFasilitator + floatval($dataPendampingFasilitator['score']);
        }

        return $scoreSikapPendampingFasilitator;
    }

    public function getFasilitatorScoreKetrampilan($participantId,$taskUuid){
        $scoreKetrampilanPendampingFasilitator = 0;
        $ketrampilanDataPendampingFasilitators = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','ketrampilan')
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        foreach($ketrampilanDataPendampingFasilitators as $ketrampilanDataPendampingFasilitator){
            $scoreKetrampilanPendampingFasilitator = $scoreKetrampilanPendampingFasilitator+floatval($ketrampilanDataPendampingFasilitator['score']);
        }

        return $scoreKetrampilanPendampingFasilitator;
    }

    public function getFasilitatorScorePengetahuan($participantId,$taskUuid){
        $scorePengetahuan = 0;
        $pengetahuan = AssessmentsModel::where('task_uuid','=',$taskUuid)
            ->where('participant_id','=',$participantId)
            ->where('assessment_type','=','pengetahuan')
            ->select('score')
            ->groupBy('assessment_element_uuid')
            ->get();
        foreach($pengetahuan as $pengetahuan){
            $scorePengetahuan = $scorePengetahuan+floatval($pengetahuan['score']);
        }

        return $scorePengetahuan;
    }



    public function getAssessmentResultFasilitatorTraveling($participantId, $taskUuid)
    {
        $scoreSikap = $this->getFasilitatorScoreSikap($participantId,$taskUuid);
        $scoreKetrampilan = $this->getFasilitatorScoreKetrampilan($participantId,$taskUuid);
        $scorePengetahuan = $this->getFasilitatorScorePengetahuan($participantId,$taskUuid);

        return [
            'scoreSikap'=>$scoreSikap,
            'scoreKetrampilan'=>$scoreKetrampilan,
            'scorePengetahuan'=>$scorePengetahuan,
            'total'=>round($scoreSikap+$scoreKetrampilan+$scorePengetahuan,1)
        ];
    }


}