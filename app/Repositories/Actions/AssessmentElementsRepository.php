<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:26
 */

namespace App\Repositories\Actions;


use App\Models\AssessmentElementsModel;
use App\Repositories\Contracts\IAssessmentElementsRepository;

class AssessmentElementsRepository implements IAssessmentElementsRepository
{

    public function isAssessmentElementExist($assessmentCategoryUuid,$element, $uuid = null)
    {
        $query = AssessmentElementsModel::query();
        $query->where('element','=',$element)->where('assessment_category_uuid','=',$assessmentCategoryUuid);

        if($uuid != null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count() > 0);
    }

    public function create($input)
    {
        $assessementElement = new AssessmentElementsModel();
        $assessementElement->assessment_category_uuid = $input['assessmentCategoryUuid'];
        $assessementElement->element = $input['assessmentElement'];
        $assessementElement->integrity = $input['elementIntegrity'];
        $assessementElement->save();

        return $assessementElement->uuid;
    }

    public function update($input)
    {
        $assessementElement = AssessmentElementsModel::uuid($input['uuid']);
        $assessementElement->element = $input['assessmentElement'];
        $assessementElement->integrity = $input['elementIntegrity'];
        $assessementElement->save();

        return $assessementElement->uuid;
    }

    public function delete($id)
    {
        return AssessmentElementsModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $assessmentElement = AssessmentElementsModel::uuid($id);

        return [
            'uuid'=>$assessmentElement->uuid,
            'assessmentElement'=>$assessmentElement->element,
            'elementIntegrity'=>$assessmentElement->integrity,
        ];
    }

    public function showAll()
    {
        // TODO: Implement showAll() method.
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function readByAssessmentCategory($assessmentCategoryUuid)
    {
        $assessmentElements = AssessmentElementsModel::where('assessment_category_uuid','=',$assessmentCategoryUuid)->get();
        $data =[];

        foreach ($assessmentElements as $assessmentElement){
            $data[]=[
                'uuid'=>$assessmentElement->uuid,
                'assessmentCategoryUuid'=>$assessmentElement->assessment_category_uuid,
                'assessmentElement'=>$assessmentElement->element,
                'elementIntegrity'=>$assessmentElement->integrity,
            ];
        }

        return $data;
    }

    public function massDelete($assessmentCategoryUuid, $uuids)
    {
        return AssessmentElementsModel::where('assessment_category_uuid','=',$assessmentCategoryUuid)
            ->whereNotIn('uuid',$uuids)
            ->delete();
    }

    public function deleteByAssessmentCategory($assessmentCategoryUuid)
    {
        return AssessmentElementsModel::where('assessment_category_uuid','=',$assessmentCategoryUuid)->delete();
    }


}