<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:48
 */

namespace App\Repositories\Actions;


use App\Models\AssessmentElementScalesModel;
use App\Repositories\Contracts\IAssessmentElementScalesRepository;

class AssessmentElementScalesRepository implements IAssessmentElementScalesRepository
{

    public function isElementScaleExist($elementUuid,$textScale, $valueScale, $uuid = null)
    {
        $query = AssessmentElementScalesModel::query();
        $query->where(function ($q)use($textScale,$valueScale){
            $q->where('text_scale','=',$textScale)
                ->where('value_scale','=',$valueScale);
        })->where('assessment_element_uuid','=',$elementUuid);

        if($uuid != null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count()>0);
    }

    public function create($input)
    {
        $assessmentElementScale = new AssessmentElementScalesModel();
        $assessmentElementScale->assessment_element_uuid = $input['elementUuid'];
        $assessmentElementScale->text_scale = $input['textScale'];
        $assessmentElementScale->value_scale = $input['valueScale'];

        return $assessmentElementScale->save();
    }

    public function update($input)
    {
        $assessmentElementScale = AssessmentElementScalesModel::find($input['uuid']);
        $assessmentElementScale->text_scale = $input['textScale'];
        $assessmentElementScale->value_scale = $input['valueScale'];

        return $assessmentElementScale->save();
    }

    public function delete($id)
    {
        return AssessmentElementScalesModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $assessmentElementScale = AssessmentElementScalesModel::uuid($id);

        return [
            'uuid'=>$assessmentElementScale->uuid,
            'textScale'=>$assessmentElementScale->text_scale,
            'valueScale'=>$assessmentElementScale->value_scale
        ];
    }

    public function showAll()
    {
        // TODO: Implement showAll() method.
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function getByAssessmentElement($elementUuid)
    {
        $assessmentElementScales = AssessmentElementScalesModel::where('assessment_element_uuid','=',$elementUuid)
            ->orderBy('value_scale','asc')
            ->get();
        $data = [];

        foreach ($assessmentElementScales as $assessmentElementScale){
            $data[] = [
                'assessmentElementUuid'=>$assessmentElementScale->assessment_element_uuid,
                'uuid'=>$assessmentElementScale->uuid,
                'textScale'=>$assessmentElementScale->text_scale,
                'valueScale'=>$assessmentElementScale->value_scale
            ];
        }

        return $data;
    }

    public function massDelete($elementUuid, $uuids)
    {
        return AssessmentElementScalesModel::where('assessment_element_uuid','=',$elementUuid)
            ->whereNotIn('uuid',$uuids)
            ->delete();
    }

    public function deleteByElementUuid($elementUuid)
    {
        return AssessmentElementScalesModel::where('assessment_element_uuid','=',$elementUuid)->delete();
    }


}