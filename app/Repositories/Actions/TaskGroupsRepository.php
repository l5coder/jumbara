<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-02
 * Time: 02:48
 */

namespace App\Repositories\Actions;


use App\Models\ParticipantGroupsModel;
use App\Models\TaskGroupsModel;
use App\Models\TaskModel;
use App\Repositories\Contracts\ITaskGroupsRepository;

class TaskGroupsRepository implements ITaskGroupsRepository
{

    public function create($input)
    {
        $taskGroup = new TaskGroupsModel();
        $taskGroup->group_name = $input['groupName'];
        $taskGroup->task_uuid = $input['taskUuid'];
        $taskGroup->save();

        return $taskGroup->uuid;
    }

    public function update($input)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        return TaskGroupsModel::uuid($id)->delete();
    }

    public function read($id)
    {

    }

    public function showAll()
    {
        $data = [];
        $taskGroups = ParticipantGroupsModel::join('task_groups','task_groups.uuid','=','participant_groups.group_uuid')
            ->join('tasks','tasks.uuid','=','participant_groups.task_uuid')
            ->join('participant','participant.id','=','participant_groups.participant_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('position','position.id','=','participant.position_id')
            ->select('participant_groups.uuid','participant.full_name','contingent.contingent_name','position.position_name','participant_groups.group_uuid',
                'task_groups.group_name','tasks.task_title')
            ->get();

        foreach ($taskGroups as $taskGroup){
            $data[]=[
                'uuid'=>$taskGroup->uuid,
                'groupUuid'=>$taskGroup->group_uuid,
                'groupName'=>$taskGroup->group_name,
                'taskUuid'=>$taskGroup->task_uuid,
                'taskTitle'=>$taskGroup->task_title,
                'fullName'=>$taskGroup->full_name,
                'contingentName'=>$taskGroup->contingent_name,
                'positionName'=>$taskGroup->position_name
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function getByTask($taskUuid)
    {
        $taskGroups = TaskGroupsModel::join('tasks','tasks.uuid','=','task_groups.task_uuid')
            ->select('task_groups.*','tasks.task_title')
            ->where('task_uuid','=',$taskUuid)
            ->get();
        $data = [];

        foreach ($taskGroups as $taskGroup){
            $data[]=[
                'uuid'=>$taskGroup->uuid,
                'groupName'=>$taskGroup->group_name,
                'taskUuid'=>$taskGroup->task_uuid,
                'taskTitle'=>$taskGroup->task_title
            ];
        }

        return $data;
    }

    public function getCountGroup($taskUuid)
    {
        $result = TaskGroupsModel::select('task_uuid')->where('task_uuid','=',$taskUuid)->distinct('task_uuid')->count();

        return $result;
    }


}