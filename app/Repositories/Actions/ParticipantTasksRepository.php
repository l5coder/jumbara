<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-22
 * Time: 04:05
 */

namespace App\Repositories\Actions;


use App\Models\ParticipantModel;
use App\Models\ParticipantTasksModel;
use App\Repositories\Contracts\IParticipantTasksRepository;

class ParticipantTasksRepository implements IParticipantTasksRepository
{

    public function create($input)
    {
        $participantTask = new ParticipantTasksModel();
        $participantTask->participant_id = $input['participantId'];
        $participantTask->task_uuid = $input['taskUuid'];

        return $participantTask->save();
    }

    public function update($input)
    {
        return ParticipantTasksModel::where('participant_id','=',$input['participantId'])
            ->where('task_uuid','=',$input['taskUuid'])
            ->update(['task_uuid'=>$input['taskUuid']]);
    }

    public function delete($id)
    {
        return null;
    }

    public function read($id)
    {
        // TODO: Implement read() method.
    }

    public function showAll()
    {
        // TODO: Implement showAll() method.
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function getParticipantTasksByContingent($contingentId)
    {
        $participants = ParticipantModel::join('position','position.id','=','participant.position_id')
            ->select('participant.id','participant.participant_id','participant.participant_number','participant.contingent_id','participant.position_id',
                'participant.full_name','participant.mis','position.position_name')
            ->where('participant.contingent_id','=',$contingentId)
            ->whereNotIn('participant.position_id',[46,47])
            ->get();
        $data =[];

        foreach($participants as $participant){
            $participantTasksCount = ParticipantTasksModel::join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
                ->select('tasks.task_title','participant_tasks.*')
                ->where('participant_id','=',$participant->id)
                ->count();

            if($participantTasksCount){
                $participantTasks = ParticipantTasksModel::join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
                    ->select('tasks.task_title','participant_tasks.*')
                    ->where('participant_id','=',$participant->id)
                    ->get();
                $participantTaskData = [];
                foreach($participantTasks as $participantTask){
                    $participantTaskData[]=[
                        'uuid'=>$participantTask->uuid,
                        'taskUuid'=>$participantTask->task_uuid,
                        'taskTitle'=>$participantTask->task_title
                    ];
                }
                $data[]=[
                    'participantId'=>$participant->id,
                    'fullName'=>$participant->full_name,
                    'participantIdCard'=>$participant->participant_id.''.$participant->participant_number,
                    'positionName'=>$participant->position_name,
                    'positionId'=>$participant->position_id,
                    'tasks'=>$participantTaskData
                ];
            }
        }

        return $data;
    }

    public function deleteTask($participantId, $taskUuid)
    {
        return ParticipantTasksModel::whereNotIn('participant_id',$participantId)
            ->whereNotIn('task_uuid',$taskUuid)
            ->delete();
    }

    public function isParticipantTaskExist($participantId, $taskUuid, $uuid = null)
    {
        $query = ParticipantTasksModel::query();
        $query->where(function($q)use($participantId,$taskUuid){
            $q->where('participant_id','=',$participantId)
                ->where('task_uuid','=',$taskUuid);
        });

        if($uuid != null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count()>0);
    }

    public function isParticipantHasTask($participantId)
    {
        $result = ParticipantTasksModel::where('participant_id','=',$participantId)->count();

        return ($result>0);
    }

    public function deleteByParticipant($participantId)
    {
        return ParticipantTasksModel::where('participant_id','=',$participantId)->delete();
    }

    public function getAllParticipantTravelingTask($taskUuid)
    {
        $query = ParticipantTasksModel::query();
        $query->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
            ->join('participant','participant.id','=','participant_tasks.participant_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->select('participant.full_name','position.position_name','tasks.task_title','participant.id','participant.participant_id',
                'participant.participant_number','participant_tasks.task_uuid','contingent.contingent_name')
            ->where('participant_tasks.task_uuid','=',$taskUuid);

        $data = [];
        $participantTasks = $query->get();
        foreach ($participantTasks as $participantTask){
            $data[]=[
                'id'=>$participantTask['id'],
                'participantId'=>$participantTask['participant_id'].''.$participantTask['participant_number'],
                'fullName'=>$participantTask['full_name'],
                'positionName'=>$participantTask['position_name'],
                'contingentName'=>$participantTask['contingent_name'],
                'taskTitle'=>$participantTask['task_title'],
                'taskUuid'=>$participantTask['task_uuid'],
                'nilai'=>0
            ];
        }

        return $data;
    }

    public function getTaskTravelingParticipantPmr()
    {
        $participantTasks = ParticipantTasksModel::join('participant','participant.id','=','participant_tasks.participant_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
            ->select('participant_tasks.participant_id','participant.full_name','participant.contingent_id','participant.position_id','contingent.contingent_name',
                'position.position_name', 'participant_tasks.task_uuid','tasks.task_title')
            ->where('tasks.is_traveling',1)
            ->whereIn('participant.position_id',[17,18,19])
            ->get();
        $data = [];
        foreach ($participantTasks as $participantTask) {
            $data[]=[
                'participantId'=>$participantTask->participant_id,
                'fullName'=>$participantTask->full_name,
                'contingentName'=>$participantTask->contingent_name,
                'contingentId'=>$participantTask->contingent_id,
                'positionName'=>$participantTask->position_name,
                'positionId'=>$participantTask->position_id,
                'taskUuid'=>$participantTask->task_uuid,
                'taskTitle'=>$participantTask->task_title
            ];
        }

        return $data;
    }

    public function getTaskNonTravelingParticipantPmr()
    {
        $participantTasks = ParticipantTasksModel::join('participant','participant.id','=','participant_tasks.participant_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
            ->select('participant_tasks.participant_id','participant.full_name','participant.contingent_id','participant.position_id','contingent.contingent_name',
                'position.position_name', 'participant_tasks.task_uuid','tasks.task_title')
            ->where('tasks.is_traveling',0)
            ->whereIn('participant.position_id',[17,18,19])
            ->get();
        $data = [];
        foreach ($participantTasks as $participantTask) {
            $data[]=[
                'participantId'=>$participantTask->participant_id,
                'fullName'=>$participantTask->full_name,
                'contingentName'=>$participantTask->contingent_name,
                'contingentId'=>$participantTask->contingent_id,
                'positionName'=>$participantTask->position_name,
                'positionId'=>$participantTask->position_id,
                'taskUuid'=>$participantTask->task_uuid,
                'taskTitle'=>$participantTask->task_title
            ];
        }

        return $data;
    }

    public function getTaskTravelingFasilitator()
    {
        $participantTasks = ParticipantTasksModel::join('participant','participant.id','=','participant_tasks.participant_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
            ->select('participant_tasks.participant_id','participant.full_name','participant.contingent_id','participant.position_id','contingent.contingent_name',
                'position.position_name', 'participant_tasks.task_uuid','tasks.task_title')
            ->where('tasks.is_traveling',1)
            ->where('participant.position_id','=',35)
            ->get();
        $data = [];
        foreach ($participantTasks as $participantTask) {
            $data[]=[
                'participantId'=>$participantTask->participant_id,
                'fullName'=>$participantTask->full_name,
                'contingentName'=>$participantTask->contingent_name,
                'contingentId'=>$participantTask->contingent_id,
                'positionName'=>$participantTask->position_name,
                'positionId'=>$participantTask->position_id,
                'taskUuid'=>$participantTask->task_uuid,
                'taskTitle'=>$participantTask->task_title
            ];
        }

        return $data;
    }

    public function getPmrParticipantTaskByContingent($contingentId, $isTraveling)
    {
        $participantTasks = ParticipantTasksModel::join('participant','participant.id','=','participant_tasks.participant_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
            ->select('participant_tasks.participant_id','participant.full_name','participant.contingent_id','participant.position_id',
                'position.position_name', 'participant_tasks.task_uuid','tasks.task_title')
            ->where('tasks.is_traveling','=',$isTraveling)
            ->where('participant.contingent_id','=',$contingentId)
            ->whereNotIn('participant.position_id',[35,49])
            ->get();
        $data = [];
        foreach ($participantTasks as $participantTask) {
            $data[]=[
                'participantId'=>$participantTask->participant_id,
                'fullName'=>$participantTask->full_name,
                'contingentId'=>$participantTask->contingent_id,
                'positionName'=>$participantTask->position_name,
                'positionId'=>$participantTask->position_id,
                'taskUuid'=>$participantTask->task_uuid,
                'taskTitle'=>$participantTask->task_title
            ];
        }

        return $data;
    }

    public function getFasilitatorParticipantTaskByContingent($contingentId, $isTraveling)
    {
        $participantTasks = ParticipantTasksModel::join('participant','participant.id','=','participant_tasks.participant_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('tasks','tasks.uuid','=','participant_tasks.task_uuid')
            ->select('participant_tasks.participant_id','participant.full_name','participant.contingent_id','participant.position_id',
                'position.position_name', 'participant_tasks.task_uuid','tasks.task_title')
            ->where('tasks.is_traveling','=',$isTraveling)
            ->where('participant.contingent_id','=',$contingentId)
            ->where('participant.position_id','=',35)
            ->get();
        $data = [];
        foreach ($participantTasks as $participantTask) {
            $data[]=[
                'participantId'=>$participantTask->participant_id,
                'fullName'=>$participantTask->full_name,
                'contingentId'=>$participantTask->contingent_id,
                'positionName'=>$participantTask->position_name,
                'positionId'=>$participantTask->position_id,
                'taskUuid'=>$participantTask->task_uuid,
                'taskTitle'=>$participantTask->task_title
            ];
        }

        return $data;
    }
}