<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-04
 * Time: 00:28
 */

namespace App\Repositories\Actions;


use App\Models\JudgeTaskGroupsModel;
use App\Repositories\Contracts\IJudgeTaskGroupsRepository;

class JudgeTaskGroupsRepository implements IJudgeTaskGroupsRepository
{

    public function create($input)
    {
        $judgeTaskGroup = new JudgeTaskGroupsModel();
        $judgeTaskGroup->user_id = $input['userId'];
        $judgeTaskGroup->task_uuid  = $input['taskUuid'];
        $judgeTaskGroup->group_uuid = $input['groupUuid'];
        $judgeTaskGroup->judging_type = $input['judgingType'];

        return $judgeTaskGroup->save();
    }

    public function update($input)
    {
        $judgeTaskGroup = judgeTaskGroupsModel::find($input['uuid']);
        $judgeTaskGroup->user_id = $input['userId'];
        $judgeTaskGroup->task_uuid  = $input['taskUuid'];
        $judgeTaskGroup->group_uuid = $input['groupUuid'];
        $judgeTaskGroup->judging_type = $input['judgingType'];

        return $judgeTaskGroup->save();
    }

    public function delete($id)
    {
        return JudgeTaskGroupsModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $judgeTaskGroup = JudgeTaskGroupsModel::join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
            ->join('participant','participant.user_id','=','judge_task_groups.user_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('task_groups','task_groups.uuid','=','judge_task_groups.group_uuid')
            ->select('judge_task_groups.*','tasks.task_title','participant.full_name','participant.id as participant_id','position.position_name',
                'contingent.contingent_name','task_groups.group_name')
            ->where('judge_task_groups.uuid','=',$id)
            ->first();

        return [
            'uuid'=>$judgeTaskGroup->uuid,
            'taskUuid'=>$judgeTaskGroup->task_uuid,
            'groupUuid'=>$judgeTaskGroup->group_uuid,
            'groupName'=>$judgeTaskGroup->group_name,
            'userId'=>$judgeTaskGroup->user_id,
            'judgingType'=>$judgeTaskGroup->judging_type,
            'taskTitle'=>$judgeTaskGroup->task_title,
            'participantId'=>$judgeTaskGroup->participant_id,
            'fullName'=>$judgeTaskGroup->full_name,
            'contingentName'=>$judgeTaskGroup->contingent_name,
            'positionName'=>$judgeTaskGroup->position_name
        ];
    }

    public function showAll()
    {
        $judgeTaskGroups = JudgeTaskGroupsModel::join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
            ->join('participant','participant.user_id','=','judge_task_groups.user_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('task_groups','task_groups.uuid','=','judge_task_groups.group_uuid')
            ->select('judge_task_groups.*','tasks.task_title','participant.full_name','participant.id as participant_id','position.position_name',
                'contingent.contingent_name','task_groups.group_name')
            ->get();
        $data = [];

        foreach ($judgeTaskGroups as $judgeTaskGroup){
            $data[]=[
                'uuid'=>$judgeTaskGroup->uuid,
                'taskUuid'=>$judgeTaskGroup->task_uuid,
                'groupUuid'=>$judgeTaskGroup->group_uuid,
                'groupName'=>$judgeTaskGroup->group_name,
                'userId'=>$judgeTaskGroup->user_id,
                'judgingType'=>$judgeTaskGroup->judging_type,
                'taskTitle'=>$judgeTaskGroup->task_title,
                'participantId'=>$judgeTaskGroup->participant_id,
                'fullName'=>$judgeTaskGroup->full_name,
                'contingentName'=>$judgeTaskGroup->contingent_name,
                'positionName'=>$judgeTaskGroup->position_name
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function isJudgeExistOnUserGroup($userId,$groupUuid,$taskUuid)
    {
        $result = JudgeTaskGroupsModel::where(function ($q)use($userId,$taskUuid){
            $q->where('user_id','=',$userId)->where('task_uuid','=',$taskUuid);
        })->where('group_uuid','=',$groupUuid)->count();

        return ($result>0);
    }

    public function getJudgeTask($judgeId)
    {
        $judgeTasks = JudgeTaskGroupsModel::join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
            ->select('judge_task_groups.task_uuid','tasks.task_title','tasks.is_traveling','judge_task_groups.judging_type')
            ->distinct('judge_task_groups.task_uuid')
            ->where('user_id','=',$judgeId)
            ->get();
        $data = [];

        foreach ($judgeTasks as $judgeTask){
            $data[]=[
                'uuid'=>$judgeTask->task_uuid,
                'taskTitle'=>$judgeTask->task_title,
                'isTraveling'=>$judgeTask->is_traveling
            ];
        }

        return $data;
    }

    public function getByJudgeTask($judgeId, $taskUuid)
    {
        $data = [];
        $judgeTaskGroups = JudgeTaskGroupsModel::join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
            ->join('participant','participant.user_id','=','judge_task_groups.user_id')
            ->join('contingent','contingent.id','=','participant.contingent_id')
            ->join('position','position.id','=','participant.position_id')
            ->join('task_groups','task_groups.uuid','=','judge_task_groups.group_uuid')
            ->select('judge_task_groups.*','tasks.task_title','participant.full_name','participant.id as participant_id','position.position_name',
                'contingent.contingent_name','task_groups.group_name','tasks.is_traveling','judge_task_groups.is_locked','participant.position_id')
            ->where('judge_task_groups.user_id','=',$judgeId)
            ->where('judge_task_groups.task_uuid','=',$taskUuid)
            ->get();

        foreach ($judgeTaskGroups as $judgeTaskGroup){
            $data[]=[
                'uuid'=>$judgeTaskGroup['uuid'],
                'taskUuid'=>$judgeTaskGroup['task_uuid'],
                'isTraveling'=>$judgeTaskGroup['is_traveling'],
                'groupUuid'=>$judgeTaskGroup['group_uuid'],
                'groupName'=>$judgeTaskGroup['group_name'],
                'userId'=>$judgeTaskGroup['user_id'],
                'judgingType'=>$judgeTaskGroup['judging_type'],
                'taskTitle'=>$judgeTaskGroup['task_title'],
                'participantId'=>$judgeTaskGroup['participant_id'],
                'fullName'=>$judgeTaskGroup['full_name'],
                'contingentName'=>$judgeTaskGroup['contingent_name'],
                'positionName'=>$judgeTaskGroup['position_name'],
                'judgePositionId'=>$judgeTaskGroup['position_id'],
                'isLock'=>$judgeTaskGroup['is_locked']
            ];
        }

        return $data;
    }

    public function setLock($groupUuid, $taskUuid)
    {
        return JudgeTaskGroupsModel::where('group_uuid','=',$groupUuid)->where('task_uuid','=',$taskUuid)->update(['is_locked'=>1]);
    }

    public function getGroupsByJudgeGroupsNonTraveling($judgeId)
    {
        $judgeTasks = JudgeTaskGroupsModel::join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
            ->select('judge_task_groups.task_uuid','judge_task_groups.group_uuid','tasks.task_title')
            ->where('tasks.is_traveling','=',0)
            ->where('judge_task_groups.user_id','=',$judgeId)
            ->get();
        $data = [];

        foreach ($judgeTasks as $judgeTask){
            $data[]=[
                'taskUuid'=>$judgeTask->task_uuid,
                'taskTitle'=>$judgeTask->task_title,
                'groupUuid'=>$judgeTask->group_uuid
            ];
        }

        return $data;
    }

    public function getGroupByJudgeGroupsTraveling($judgeId)
    {
        $judgeTasks = JudgeTaskGroupsModel::join('tasks','tasks.uuid','=','judge_task_groups.task_uuid')
            ->select('judge_task_groups.task_uuid','judge_task_groups.group_uuid','tasks.task_title')
            ->where('tasks.is_traveling','=',1)
            ->where('judge_task_groups.user_id','=',$judgeId)
            ->get();
        $data = [];

        foreach ($judgeTasks as $judgeTask){
            $data[]=[
                'taskUuid'=>$judgeTask->task_uuid,
                'taskTitle'=>$judgeTask->task_title,
                'groupUuid'=>$judgeTask->group_uuid
            ];
        }

        return $data;
    }


}