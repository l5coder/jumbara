<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:05
 */

namespace App\Repositories\Actions;


use App\Models\AssessmentCategoriesModel;
use App\Repositories\Contracts\IAssessmentCategoriesRepository;

class AssessmentCategoriesRepository implements IAssessmentCategoriesRepository
{

    public function isAssessmentCategoryExist($assessmentCategory,$taskUuid, $uuid = null)
    {
        $query = AssessmentCategoriesModel::query();

        $query->where(function ($q)use($assessmentCategory,$taskUuid){
            $q->where('category','=',$assessmentCategory)
                ->where('task_uuid','=',$taskUuid);
        });
        if($uuid != null){
            $query->where('uuid','<>',$uuid);
        }

        return ($query->count() > 0);
    }

    public function create($input)
    {
        $assessmentCategory = new AssessmentCategoriesModel();
        $assessmentCategory->is_traveling = $input['isTraveling'];
        $assessmentCategory->type = $input['assessmentType'];
        $assessmentCategory->category = $input['assessmentCategory'];
        $assessmentCategory->task_uuid = $input['taskUuid'];
        $assessmentCategory->participant_rank = $input['participantRank'];
        $assessmentCategory->save();

        return $assessmentCategory->uuid;
    }

    public function update($input)
    {
        $assessmentCategory = AssessmentCategoriesModel::uuid($input['uuid']);
        $assessmentCategory->is_traveling = $input['isTraveling'];
        $assessmentCategory->type = $input['assessmentType'];
        $assessmentCategory->category = $input['assessmentCategory'];
        $assessmentCategory->task_uuid = $input['taskUuid'];
        $assessmentCategory->participant_rank = $input['participantRank'];
        $assessmentCategory->save();

        return $assessmentCategory->uuid;
    }

    public function delete($id)
    {
        return AssessmentCategoriesModel::uuid($id)->delete();
    }

    public function read($id)
    {
        $assessmentCategory = AssessmentCategoriesModel::uuid($id);

        return [
            'uuid'=>$assessmentCategory->uuid,
            'isTraveling'=>$assessmentCategory->is_traveling,
            'assessmentType'=>$assessmentCategory->type,
            'assessmentCategory'=>$assessmentCategory->category,
            'taskUuid'=>$assessmentCategory->task_uuid,
            'participantRank'=>$assessmentCategory->participant_rank
        ];
    }

    public function showAll()
    {
        $assessmentCategories = AssessmentCategoriesModel::leftJoin('tasks','tasks.uuid','=','assessment_categories.task_uuid')
            ->select('assessment_categories.*','tasks.task_title')
            ->get();
        $data = [];

        foreach ($assessmentCategories as $assessmentCategory){
            $data[]=[
                'uuid'=>$assessmentCategory->uuid,
                'isTraveling'=>$assessmentCategory->is_traveling,
                'assessmentType'=>$assessmentCategory->type,
                'assessmentCategory'=>$assessmentCategory->category,
                'taskUuid'=>$assessmentCategory->task_uuid,
                'taskTitle'=>$assessmentCategory->task_title,
                'participantRank'=>$assessmentCategory->participant_rank
            ];
        }

        return $data;
    }

    public function pagination($searchPhrase = null)
    {
        // TODO: Implement pagination() method.
    }

    public function getByTravelingType($isTraveling,$assessmentType,$taskUuid = null)
    {
        if($isTraveling ==1){
            $assessmentCategories = AssessmentCategoriesModel::where('is_traveling','=',$isTraveling)
                ->where('type','=',$assessmentType)
                ->groupBy('type','uuid')
                ->get();
        }else{
            $assessmentCategories = AssessmentCategoriesModel::where('task_uuid','=',$taskUuid)
                ->where('type','=',$assessmentType)
                ->groupBy('type','uuid')
                ->get();
        }

//        $assessmentCategories = AssessmentCategoriesModel::where('task_uuid','=',$taskUuid)
//            ->where('type','=',$assessmentType)
//            ->groupBy('type','uuid')
//            ->get();

        $data = [];

        foreach ($assessmentCategories as $assessmentCategory){
            $data[]=[
                'uuid'=>$assessmentCategory->uuid,
                'isTraveling'=>$assessmentCategory->is_traveling,
                'assessmentType'=>$assessmentCategory->type,
                'assessmentCategory'=>$assessmentCategory->category,
            ];
        }

        return $data;
    }


}