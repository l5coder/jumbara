<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class ParticipantGroupsModel extends Model
{
    use Uuids;

    public $table = 'participant_groups';
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'uuid';
}
