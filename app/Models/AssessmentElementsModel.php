<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssessmentElementsModel extends Model
{
    use Uuids;

    protected $table = 'assessment_elements';
    protected $primaryKey = 'uuid';
    public $timestamps = false;
    public $incrementing = false;

}
