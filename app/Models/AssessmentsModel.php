<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssessmentsModel extends Model
{
    use Uuids;

    protected $table = 'participant_assessments';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'uuid';

}
