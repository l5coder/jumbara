<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssessmentElementScalesModel extends Model
{
    use Uuids;

    protected $table = 'assessment_element_scales';
    protected $primaryKey = 'uuid';
    public $incrementing  = false;
    public $timestamps = false;

}
