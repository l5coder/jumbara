<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class AssessmentCategoriesModel extends Model
{
    use Uuids;

    protected $table = 'assessment_categories';
    protected $primaryKey = 'uuid';
    public $timestamps = false;
    public $incrementing = false;

}
