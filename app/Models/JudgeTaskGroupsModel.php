<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class JudgeTaskGroupsModel extends Model
{
    use Uuids;

    protected $table = 'judge_task_groups';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    public $timestamps = false;
}
