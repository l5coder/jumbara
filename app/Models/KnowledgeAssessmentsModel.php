<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class KnowledgeAssessmentsModel extends Model
{
    use Uuids;

    protected $table='knowledge_assessments';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['task_uuid','participant_id','score'];
}
