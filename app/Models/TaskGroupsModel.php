<?php

namespace App\Models;

use Emadadly\LaravelUuid\Uuids;
use Illuminate\Database\Eloquent\Model;

class TaskGroupsModel extends Model
{
    use Uuids;

    protected $table='task_groups';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'uuid';
}
