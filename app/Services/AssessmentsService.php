<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-13
 * Time: 05:36
 */

namespace App\Services;


use App\Repositories\Contracts\IAssessmentsRepository;
use App\Repositories\Contracts\IJudgeTaskGroupsRepository;
use App\Repositories\Contracts\IParticipantGroupsRepository;
use App\Repositories\Contracts\IParticipantTasksRepository;
use App\Services\Response\ServiceResponseDto;

class AssessmentsService extends BaseService
{
    protected $assessmentsRepository;
    protected $judgeTaskGroupRepository;
    protected $participantGroupsRepository;
    protected $participantTasksRepository;

    public function __construct(IAssessmentsRepository $assessmentsRepository,IJudgeTaskGroupsRepository $judgeTaskGroupsRepository,
                                IParticipantGroupsRepository $participantGroupsRepository,IParticipantTasksRepository $participantTasksRepository)
    {
        $this->assessmentsRepository = $assessmentsRepository;
        $this->judgeTaskGroupRepository = $judgeTaskGroupsRepository;
        $this->participantGroupsRepository = $participantGroupsRepository;
        $this->participantTasksRepository = $participantTasksRepository;
    }

    protected function setLock($groupUuid,$taskUuid){
        $response = new ServiceResponseDto();

        try{
            $this->judgeTaskGroupRepository->setLock($groupUuid,$taskUuid);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function createAssessment($input){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->assessmentsRepository->create($input));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
//        $judgingType = $input['type'];

        $participantAssessments = $input['assessmentParticipants'];
        foreach ($participantAssessments as $participantAssessment){
            $scores = $participantAssessment['resultAssessments'];
            foreach ($scores as $score) {

                $param = [
                    'judgeId'=>$input['judgeId'],
                    'judgePositionId'=>$input['judgePositionId'],
                    'participantId'=>$participantAssessment['participantId'],
                    'taskUuid'=>$input['taskUuid'],
                    'groupUuid'=>$input['groupUuid'],
                    'type'=>$input['type'],
                    'elementUuid'=>$score['assessmentElementUuid'],
                    'assessmentCategoryUuid'=>$score['assessmentCategoryUuid'],
                    'assessmentType'=>$score['assessmentType'],
                    'scaleValue'=>$score['valueScale'],
                    'bobot'=>$score['bobot'],
                    'score'=>$score['score']
                ];
                $setLock = $this->setLock($input['groupUuid'],$input['taskUuid']);
                if($setLock->isSuccess()){
                    $createAssessment = $this->createAssessment($param);
                    if(!$createAssessment->isSuccess()){
                        $response->addErrorMessage($createAssessment->getErrorMessages());
                    }else{
                        $response->setResult($input['groupUuid']);
                    }
                }else{
                    $response->addErrorMessage($setLock->getErrorMessages());
                }
            }
        }

        return $response;
    }

    protected function getGroupsByJudgeGroupTaskNonTraveling($judgeId){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->judgeTaskGroupRepository->getGroupsByJudgeGroupsNonTraveling($judgeId));
        }catch (\Exception $exception){
            $response->addErrorMessage($response->getErrorMessages());
        }

        return $response;
    }

    protected function getParticipantFromGroup($groupUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantGroupsRepository->getParticipantByGroup($groupUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($response->getErrorMessages());
        }

        return $response;
    }

    protected function isAssessmentResultByJudgeTaskGroupExist($taskUuid,$groupUuid,$participantId,$judgeId){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->assessmentsRepository->isAssessmentResultByJudgeTaskGroupExist($taskUuid,$groupUuid,$participantId,$judgeId));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getAssessmentResultsByJudgeNonTraveling($judgeId){
        $response = new ServiceResponseDto();
        try{
            $participantData=[];
            $getGroup = $this->getGroupsByJudgeGroupTaskNonTraveling($judgeId);
            if($getGroup->isSuccess()){
                $groups = $getGroup->getResult();
                foreach($groups as $group) {
                    $getParticipantFromGroup = $this->getParticipantFromGroup($group['groupUuid']);
                    if($getParticipantFromGroup->isSuccess()){
                        $participants = $getParticipantFromGroup->getResult();
                        foreach($participants as $participant){
                            $isAssessmentExist = $this->isAssessmentResultByJudgeTaskGroupExist($group['taskUuid'],$group['groupUuid'],$participant['participantId'],$judgeId)->getResult();
                            if($isAssessmentExist){
                                $score = $this->assessmentsRepository->getAssessmentResultByJudgeNonTraveling($group['taskUuid'],$group['groupUuid'],$participant['participantId'],$judgeId);
                                $participantData[]=[
                                    'taskUuid'=>$group['taskUuid'],
                                    'taskTitle'=>$group['taskTitle'],
                                    'groupUuid'=>$group['groupUuid'],
                                    'participantId'=>$participant['participantId'],
                                    'fullName'=>$participant['fullName'],
                                    'contingentName'=>$participant['contingentName'],
                                    'scores'=>$score
                                ];
                            }
                        }
                    }else{
                        $response->addErrorMessage($getParticipantFromGroup->getErrorMessages());
                    }
                }
            }else{
                $response->addErrorMessage($getGroup->getErrorMessages());
            }
            $response->setResult($participantData);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }


        return $response;
    }

    protected function getGroupsByJudgeGroupTaskTraveling($judgeId){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->judgeTaskGroupRepository->getGroupByJudgeGroupsTraveling($judgeId));
        }catch (\Exception $exception){
            $response->addErrorMessage($response->getErrorMessages());
        }

        return $response;
    }

    public function getAssessmentResultsByJudgeTraveling($judgeId){
        $response = new ServiceResponseDto();
        try{
            $participantData=[];
            $getGroup = $this->getGroupsByJudgeGroupTaskTraveling($judgeId);
            if($getGroup->isSuccess()){
                $groups = $getGroup->getResult();
                foreach($groups as $group) {
                    $getParticipantFromGroup = $this->getParticipantFromGroup($group['groupUuid']);
                    if($getParticipantFromGroup->isSuccess()){
                        $participants = $getParticipantFromGroup->getResult();
                        foreach($participants as $participant){
                            $isAssessmentExist = $this->isAssessmentResultByJudgeTaskGroupExist($group['taskUuid'],$group['groupUuid'],$participant['participantId'],$judgeId)->getResult();
                            if($isAssessmentExist){
                                $score = $this->assessmentsRepository->getAssessmentResultByJudgeTraveling($group['taskUuid'],$group['groupUuid'],$participant['participantId'],$judgeId);
                                $participantData[]=[
                                    'taskUuid'=>$group['taskUuid'],
                                    'taskTitle'=>$group['taskTitle'],
                                    'groupUuid'=>$group['groupUuid'],
                                    'participantId'=>$participant['participantId'],
                                    'fullName'=>$participant['fullName'],
                                    'contingentName'=>$participant['contingentName'],
                                    'scores'=>$score
                                ];
                            }
                        }
                    }else{
                        $response->addErrorMessage($getParticipantFromGroup->getErrorMessages());
                    }
                }
            }else{
                $response->addErrorMessage($getGroup->getErrorMessages());
            }
            $response->setResult($participantData);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }


        return $response;
    }

    protected function getTaskTravelingParticipantPmr(){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantTasksRepository->getTaskTravelingParticipantPmr());
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function isAssessmentByParticipantTaskExist($participantId,$taskUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->assessmentsRepository->isAssessmentByParticipantTaskExist($participantId,$taskUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getAssessmentResultTravelingPmr(){
        $response = new ServiceResponseDto();
        $getParticiapantPmr = $this->getTaskTravelingParticipantPmr();
        $data = [];

        if($getParticiapantPmr->isSuccess()){
            $participants = $getParticiapantPmr->getResult();
            foreach ($participants as $participant){
                $isAssessmentByParticipantTaskExist = $this->isAssessmentByParticipantTaskExist($participant['participantId'],$participant['taskUuid']);
                if($isAssessmentByParticipantTaskExist->isSuccess()){
                    if($isAssessmentByParticipantTaskExist->getResult()){
                        $scores = $this->assessmentsRepository
                            ->getAssessmentResultByParticipantTaskTraveling($participant['participantId'],$participant['taskUuid']);
                        $data[]=[
                            'participantId'=>$participant['participantId'],
                            'fullName'=>$participant['fullName'],
                            'contingentName'=>$participant['contingentName'],
                            'contingentId'=>$participant['contingentId'],
                            'positionName'=>$participant['positionName'],
                            'positionId'=>$participant['positionId'],
                            'taskUuid'=>$participant['taskUuid'],
                            'taskTitle'=>$participant['taskTitle'],
                            'scores'=>$scores
                        ];
                    }
                }else{
                    $response->addErrorMessage($isAssessmentByParticipantTaskExist->getErrorMessages());
                }
            }
            $response->setResult($data);
        }else{
            $response->addErrorMessage($getParticiapantPmr->getErrorMessages());
        }

        return $response;
    }

    protected function getTaskNonTravelingParticipantPmr(){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantTasksRepository->getTaskNonTravelingParticipantPmr());
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getAssessmentResultNonTravelingPmr(){
        $response = new ServiceResponseDto();
        $getParticiapantPmr = $this->getTaskNonTravelingParticipantPmr();
        $data = [];

        if($getParticiapantPmr->isSuccess()){
            $participants = $getParticiapantPmr->getResult();
            foreach ($participants as $participant){
                $isAssessmentByParticipantTaskExist = $this->isAssessmentByParticipantTaskExist($participant['participantId'],$participant['taskUuid']);
                if($isAssessmentByParticipantTaskExist->isSuccess()){
                    if($isAssessmentByParticipantTaskExist->getResult()){
                        $scores = $this->assessmentsRepository
                            ->getAssessmentResultByParticipantTaskNonTraveling($participant['participantId'],$participant['taskUuid']);
                        $data[]=[
                            'participantId'=>$participant['participantId'],
                            'fullName'=>$participant['fullName'],
                            'contingentName'=>$participant['contingentName'],
                            'contingentId'=>$participant['contingentId'],
                            'positionName'=>$participant['positionName'],
                            'positionId'=>$participant['positionId'],
                            'taskUuid'=>$participant['taskUuid'],
                            'taskTitle'=>$participant['taskTitle'],
                            'scores'=>$scores
                        ];
                    }
                }else{
                    $response->addErrorMessage($isAssessmentByParticipantTaskExist->getErrorMessages());
                }
            }
            $response->setResult($data);
        }else{
            $response->addErrorMessage($getParticiapantPmr->getErrorMessages());
        }

        return $response;
    }

    protected function getParticipantFasilitatorTraveling(){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantTasksRepository->getTaskTravelingFasilitator());
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getAssessmentResultFasilitatorTraveling(){
        $response = new ServiceResponseDto();
        $getParticiapantFasilitator = $this->getParticipantFasilitatorTraveling();
        $data = [];

        if($getParticiapantFasilitator->isSuccess()){
            $participants = $getParticiapantFasilitator->getResult();
            foreach ($participants as $participant){
                $isAssessmentByParticipantTaskExist = $this->isAssessmentByParticipantTaskExist($participant['participantId'],$participant['taskUuid']);
                if($isAssessmentByParticipantTaskExist->isSuccess()){
                    if($isAssessmentByParticipantTaskExist->getResult()){
                        $scores = $this->assessmentsRepository
                            ->getAssessmentResultFasilitatorTraveling($participant['participantId'],$participant['taskUuid']);
                        $data[]=[
                            'participantId'=>$participant['participantId'],
                            'fullName'=>$participant['fullName'],
                            'contingentName'=>$participant['contingentName'],
                            'contingentId'=>$participant['contingentId'],
                            'positionName'=>$participant['positionName'],
                            'positionId'=>$participant['positionId'],
                            'taskUuid'=>$participant['taskUuid'],
                            'taskTitle'=>$participant['taskTitle'],
                            'scores'=>$scores
                        ];
                    }
                }else{
                    $response->addErrorMessage($isAssessmentByParticipantTaskExist->getErrorMessages());
                }
            }
            $response->setResult($data);
        }else{
            $response->addErrorMessage($getParticiapantFasilitator->getErrorMessages());
        }

        return $response;
    }

    protected function getPmrParticipantTaskByContingent($contingentId,$isTraveling){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantTasksRepository->getPmrParticipantTaskByContingent($contingentId,$isTraveling));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getPmrAssessmentResultByContingent($contingentId,$isTraveling){
        $response = new ServiceResponseDto();
        $data = [];
        $getPticipants = $this->getPmrParticipantTaskByContingent($contingentId,$isTraveling);
        if($getPticipants->isSuccess()){
            $participants = $getPticipants->getResult();
            foreach ($participants as $participant){
                $isAssessmentResultByParticipantExist = $this->isAssessmentByParticipantTaskExist($participant['participantId'],$participant['taskUuid']);
                if($isAssessmentResultByParticipantExist->isSuccess()){
                    if($isAssessmentResultByParticipantExist->getResult()){
                        try{
                            if($isTraveling == 0){
                                $scores = $this->assessmentsRepository
                                    ->getAssessmentResultByParticipantTaskNonTraveling($participant['participantId'],$participant['taskUuid']);
                                $data[]=[
                                    'participantId'=>$participant['participantId'],
                                    'fullName'=>$participant['fullName'],
                                    'contingentId'=>$participant['contingentId'],
                                    'positionName'=>$participant['positionName'],
                                    'positionId'=>$participant['positionId'],
                                    'taskUuid'=>$participant['taskUuid'],
                                    'taskTitle'=>$participant['taskTitle'],
                                    'scores'=>$scores
                                ];
                            }else{
                                $scores = $this->assessmentsRepository->getAssessmentResultByParticipantTaskTraveling($participant['participantId'],$participant['taskUuid']);
                                $data[]=[
                                    'participantId'=>$participant['participantId'],
                                    'fullName'=>$participant['fullName'],
                                    'contingentId'=>$participant['contingentId'],
                                    'positionName'=>$participant['positionName'],
                                    'positionId'=>$participant['positionId'],
                                    'taskUuid'=>$participant['taskUuid'],
                                    'taskTitle'=>$participant['taskTitle'],
                                    'scores'=>$scores
                                ];
                            }
                        }catch (\Exception $exception){
                            $response->addErrorMessage($exception->getMessage());
                        }
                    }
                }else{
                    $response->addErrorMessage($isAssessmentResultByParticipantExist->getErrorMessages());
                }
            }
            $response->setResult($data);
        }else{
            $response->addErrorMessage($getPticipants->getErrorMessages());
        }
        return $response;
    }

    protected function getFasilitatorParticipantTaskByContingent($contingentId,$isTraveling){
        $response = new ServiceResponseDto();
        try{
            $response->setResult($this->participantTasksRepository->getFasilitatorParticipantTaskByContingent($contingentId,$isTraveling));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getFasilitatorAssessmentResultByContingent($contingentId,$isTraveling){
        $response = new ServiceResponseDto();
        $data = [];
        $getParticipants = $this->getFasilitatorParticipantTaskByContingent($contingentId,$isTraveling);

        try{
            if($getParticipants->isSuccess()){
                $participants = $getParticipants->getResult();
                foreach ($participants as $participant){
                    $isAssessmentResultByParticipantTaskExist = $this->isAssessmentByParticipantTaskExist($participant['participantId'],$participant['taskUuid'])->getResult();
                    if($isAssessmentResultByParticipantTaskExist){
                        if($isTraveling != 0){
                            $scores = $this->assessmentsRepository->getAssessmentResultFasilitatorTraveling($participant['participantId'],$participant['taskUuid']);
                            $data[]=[
                                'participantId'=>$participant['participantId'],
                                'fullName'=>$participant['fullName'],
                                'contingentId'=>$participant['contingentId'],
                                'positionName'=>$participant['positionName'],
                                'positionId'=>$participant['positionId'],
                                'taskUuid'=>$participant['taskUuid'],
                                'taskTitle'=>$participant['taskTitle'],
                                'scores'=>$scores
                            ];
                        }
                    }
                }
                $response->setResult($data);
            }else{
                $response->addErrorMessage($getParticipants->getErrorMessages());
            }
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }
}