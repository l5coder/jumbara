<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-02
 * Time: 03:09
 */

namespace App\Services;


use App\Repositories\Contracts\IParticipantGroupsRepository;
use App\Repositories\Contracts\ITaskGroupsRepository;
use App\Services\Response\ServiceResponseDto;

class GroupsService extends BaseService
{
    protected $taskGroupsRepository;
    protected $participantGroupsRepository;

    public function __construct(ITaskGroupsRepository $taskGroupsRepository,IParticipantGroupsRepository $participantGroupsRepository)
    {
        $this->taskGroupsRepository = $taskGroupsRepository;
        $this->participantGroupsRepository = $participantGroupsRepository;
    }

    protected function createTaskGroup($input){
        $response = new ServiceResponseDto();

        try{
            $groupCount = $this->getTaskGroupCount($input['taskUuid'])->getResult();
            $input['groupName'] ='Kelompok '.(intval($groupCount)+1);
            $response->setResult($this->taskGroupsRepository->create($input));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function getTaskGroupCount($taskUuid){
        $response = new ServiceResponseDto();

        $response->setResult($this->taskGroupsRepository->getCountGroup($taskUuid));

        return $response;
    }

    protected function isParticipantExistInOtherGroup($participantId,$taskUuid,$groupUuid){
        $response = new ServiceResponseDto();

        $response->setResult($this->participantGroupsRepository->isParticipantExistInOtherGroup($participantId,$taskUuid,$groupUuid));

        return $response;
    }

    protected function createParticipantGroup($input){
        $response = new ServiceResponseDto();
        $isParticipantExistInOtherGroup = $this->isParticipantExistInOtherGroup($input['participantId'],$input['taskUuid'],$input['groupUuid'])->getResult();

        if($isParticipantExistInOtherGroup){
            $response->addErrorMessage('Peserta yang anda masukkan sudah ada dalam kelompok lain dalam bidang yang sama');
        }else{
            try{
                $this->participantGroupsRepository->create($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $createGroup = $this->createTaskGroup($input);

        if($createGroup->isSuccess()){
            $groupUuid = $createGroup->getResult();
            $participants = $input['participants'];
            foreach ($participants as $participant){
                $param = [
                    'participantId'=>$participant['participantId'],
                    'taskUuid'=>$input['taskUuid'],
                    'groupUuid'=>$groupUuid
                ];
                $createParticipantGroup = $this->createParticipantGroup($param);
                if(!$createParticipantGroup->isSuccess()){
                    $response->addErrorMessage($createParticipantGroup->getErrorMessages());
                }
            }
        }else{
            $response->addErrorMessage($createGroup->getErrorMessages());
        }

        return $response;
    }

    public function showAll(){
//        $response = new ServiceResponseDto();
//        $data= [];
//        $taskGroups = [];
//        try{
//            $taskGroups = $this->taskGroupsRepository->showAll();
//            foreach ($taskGroups as $taskGroup){
//                $groupUuid = $taskGroup['uuid'];
//                $participantGroupsData = [];
//                $participantGroups = $this->participantGroupsRepository->getByGroup($groupUuid);
//                foreach ($participantGroups as $participantGroup){
//                    $participantGroupsData[]=[
//                        'uuid'=>$participantGroup['uuid'],
//                        'participantId'=>$participantGroup['participantId'],
//                        'fullName'=>$participantGroup['fullName'],
//                        'contingentName'=>$participantGroup['contingentName'],
//                        'positionName'=>$participantGroup['positionName']
//                    ];
//                }
//                $data[]=[
//                    'uuid'=>$taskGroup['uuid'],
//                    'group'=>$taskGroup['group'],
//                    'taskUuid'=>$taskGroup['taskUuid'],
//                    'taskTitle'=>$taskGroup['taskTitle'],
//                    'participants'=>$participantGroupsData
//                ];
//            }
//            $response->setResult($data);
//        }catch (\Exception $exception){
//            $response->addErrorMessage($exception->getMessage());
//        }
//
//        return $response;

        return $this->getAllObject($this->taskGroupsRepository);
    }

    public function delete($uuid){
        $response = new ServiceResponseDto();

        try{
            $participantGroup = $this->participantGroupsRepository->read($uuid);
            $groupUuid = $participantGroup['group_uuid'];

            $this->participantGroupsRepository->delete($uuid);
            $isParticipantGroupExist = $this->participantGroupsRepository->isParticipantGroupExist($groupUuid);
            if(!$isParticipantGroupExist){
                $this->taskGroupsRepository->delete($groupUuid);
            }
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }


        return $response;
    }

    public function getByTask($taskUuid){
        $response = new ServiceResponseDto();

        $response->setResult($this->taskGroupsRepository->getByTask($taskUuid));

        return $response;
    }

    public function getByGroup($groupUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantGroupsRepository->getByGroup($groupUuid));
        }catch (\Exception $exception){
            $response->setResult($exception);
        }

        return $response;
    }

}