<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-22
 * Time: 04:19
 */

namespace App\Services;


use App\Repositories\Contracts\IParticipantTasksRepository;
use App\Services\Response\ServiceResponseDto;

class ParticipantTasksService extends BaseService
{
    protected $participantTasksRepository;

    public  function __construct(IParticipantTasksRepository $participantTasksRepository)
    {
        $this->participantTasksRepository = $participantTasksRepository;
    }

    protected function isParticipantTaskExist($participantId,$taskUuid,$uuid = null){
        $response = new ServiceResponseDto();

        $response->setResult($this->participantTasksRepository->isParticipantTaskExist($participantId,$taskUuid,$uuid));

        return $response;
    }

    protected function isParticipantHasTask($participantId){
        $response = new ServiceResponseDto();

        $response->setResult($this->participantTasksRepository->isParticipantHasTask($participantId));

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();
        $isParticipantHasTask = $this->isParticipantHasTask($input['participantId'])->getResult();

        if($isParticipantHasTask){
            $response->addErrorMessage('Peserta sudah memiliki tugas, untuk menambahkan tugas pada peserta anda bisa mengubah data penugasan peserta');
        }else{
            try{
                $tasks = $input['tasks'];
                foreach ($tasks as $task){
                    $isParticipantTaskExist = $this->isParticipantTaskExist($input['participantId'],$task['taskUuid'])->getResult();
                    if(!$isParticipantTaskExist){
                        $param = [
                            'participantId'=>$input['participantId'],
                            'taskUuid'=>$task['taskUuid']
                        ];
                        $this->participantTasksRepository->create($param);
                    }else{
                        $response->addErrorMessage('Tugas untuk peserta sudah ada');
                    }
                }
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    public function showAll(){

    }

    public function getParticipantTaskByContingent($contingentId){
        $response = new ServiceResponseDto();

        $response->setResult($this->participantTasksRepository->getParticipantTasksByContingent($contingentId));

        return $response;
    }

    public function deleteByParticipant($participantId){
        $response = new ServiceResponseDto();

        try{
            $this->participantTasksRepository->deleteByParticipant($participantId);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getAllParticipantTravelingTask($taskUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantTasksRepository->getAllParticipantTravelingTask($taskUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

}