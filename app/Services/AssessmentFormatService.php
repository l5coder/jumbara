<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-05-30
 * Time: 19:58
 */

namespace App\Services;


use App\Repositories\Contracts\IAssessmentCategoriesRepository;
use App\Repositories\Contracts\IAssessmentElementScalesRepository;
use App\Repositories\Contracts\IAssessmentElementsRepository;
use App\Repositories\Contracts\IAssessmentsRepository;
use App\Repositories\Contracts\IParticipantGroupsRepository;
use App\Services\Response\ServiceResponseDto;

class AssessmentFormatService extends BaseService
{
    protected $assessmentCategoriesRepository;
    protected $assessmentElementsRepository;
    protected $assessmentElementScalesRepository;
    protected $participantGroupsRepository;
    protected $assessmentRepository;

    public function __construct(IAssessmentCategoriesRepository $assessmentCategoriesRepository, IAssessmentElementsRepository $assessmentElementsRepository,
                                IAssessmentElementScalesRepository $assessmentElementScalesRepository,IParticipantGroupsRepository $participantGroupsRepository,
IAssessmentsRepository $assessmentsRepository)
    {
        $this->assessmentCategoriesRepository = $assessmentCategoriesRepository;
        $this->assessmentElementsRepository = $assessmentElementsRepository;
        $this->assessmentElementScalesRepository = $assessmentElementScalesRepository;
        $this->participantGroupsRepository = $participantGroupsRepository;
        $this->assessmentRepository = $assessmentsRepository;
    }

    protected function isAssessmentCategoryExist($category,$uuid = null){
        $response = new ServiceResponseDto();

        $response->setResult($this->assessmentCategoriesRepository->isAssessmentCategoryExist($category,$uuid));

        return $response;
    }

    protected function isAssessmentElementExist($assessmentCategoryUuid,$element,$uuid = null){
        $response = new ServiceResponseDto();

        $response->setResult($this->assessmentElementsRepository->isAssessmentElementExist($assessmentCategoryUuid,$element,$uuid));

        return $response;
    }

    protected function isAssessmentElementScaleExist($elementUuid,$textScale, $valueScale, $uuid = null){
        $response = new ServiceResponseDto();

        $response->setResult($this->assessmentElementScalesRepository->isElementScaleExist($elementUuid,$textScale,$valueScale,$uuid));

        return $response;
    }

    protected function createAssessmentCategory($input){
        $response = new ServiceResponseDto();
        $isAssessmentCategoryExist = $this->isAssessmentCategoryExist($input['assessmentCategory'],$input['taskUuid'])->getResult();

        if($isAssessmentCategoryExist){
            $response->addErrorMessage('Kategori unsur yang di nilai sudah ada');
        }else{
            try{
                $response->setResult($this->assessmentCategoriesRepository->create($input));
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    protected function createAssessmentElement($input){
        $response = new ServiceResponseDto();
        $isAssessmentElementExist = $this->isAssessmentElementExist($input['assessmentCategoryUuid'],$input['assessmentElement'])->getResult();

        if($isAssessmentElementExist){
            $response->addErrorMessage('Unsur penilaian sudah ada');
        }else{
            try{
                $response->setResult($this->assessmentElementsRepository->create($input));
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    protected function createAssessmentElementScael($input){
        $response = new ServiceResponseDto();
        $isAssessmentElementScaleExist = $this->isAssessmentElementScaleExist($input['elementUuid'],$input['textScale'],$input['valueScale'])->getResult();

        if($isAssessmentElementScaleExist){
            $response->addErrorMessage('Bobot penilaian sudah ada');
        }else{
            try{
                $this->assessmentElementScalesRepository->create($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    public function create($input){
        $response = new ServiceResponseDto();

        //createAssessmentCategory
        $createAssessmentCategory = $this->createAssessmentCategory($input);
        if($createAssessmentCategory->isSuccess()){
            //createAssessmentElement
           $assessmentElementInputs = $input['assessmentElements'];
           $assessmentCategoryUuid = $createAssessmentCategory->getResult();
           $assessmentElementCount = 0;
           foreach ($assessmentElementInputs as $assessmentElementInput){
               $assessmentElementCount = $assessmentElementCount+1;
               $param=[
                   'assessmentCategoryUuid'=>$assessmentCategoryUuid,
                   'assessmentElement'=>$assessmentElementInput['assessmentElement'],
                   'elementIntegrity'=>$assessmentElementInput['elementIntegrity']
               ];
               $createAssessmentElement = $this->createAssessmentElement($param);
               if($createAssessmentElement->isSuccess()){
                   //createAssessmentScale
                   $assessmentElementUuid = $createAssessmentElement->getResult();
                   $assessmentElementScaleInputs = $assessmentElementInput['assessmentElementScales'];
                   $assessmentElementScaleCount = 0;
                   foreach ($assessmentElementScaleInputs as $assessmentElementScaleInput){
                       $assessmentElementScaleCount = $assessmentElementScaleCount+1;
                       $param =[
                           'elementUuid'=>$assessmentElementUuid,
                           'textScale'=>$assessmentElementScaleInput['textScale'],
                           'valueScale'=>$assessmentElementScaleInput['valueScale']
                       ];
                       $createAssessmentElementScale = $this->createAssessmentElementScael($param);
                       if(!$createAssessmentElementScale->isSuccess()){
                           $response->addErrorMessage($createAssessmentElementScale->getErrorMessages());
                       }
                   }
               }else{
                   $response->addErrorMessage($createAssessmentElement->getErrorMessages());
               }
           }
        }else{
            $response->addErrorMessage($createAssessmentCategory->getErrorMessages());
        }

        return $response;
    }

    protected function updateAssessmentCategory($input){
        $response = new ServiceResponseDto();
        $isAssessmentCategoryExist = $this->isAssessmentCategoryExist($input['assessmentCategory'],$input['uuid'])->getResult();

        if($isAssessmentCategoryExist){
            $response->addErrorMessage('Kategori unsur yang di nilai sudah ada');
        }else{
            try{
                $this->assessmentCategoriesRepository->update($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    protected function updateAssessmentElement($input){
        $response = new ServiceResponseDto();
        $isAssessmentElementExist = $this->isAssessmentElementExist($input['assessmentCategoryUuid'],$input['assessmentElement'],$input['uuid'])->getResult();

        if($isAssessmentElementExist){
            $response->addErrorMessage('Unsur penilaian sudah ada');
        }else{
            try{
                $this->assessmentElementsRepository->update($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    protected function updateAssessmentElementScale($input){
        $response = new ServiceResponseDto();
        $isAssessmentElementScaleExist = $this->isAssessmentElementScaleExist($input['elementUuid'],$input['textScale'],$input['valueScale'],$input['uuid'])->getResult();

        if($isAssessmentElementScaleExist){
            $response->addErrorMessage('Bobot penilaian sudah ada');
        }else{
            try{
                $this->assessmentElementScalesRepository->update($input);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }

        return $response;
    }

    public function update($input,$uuid){
        $response = new ServiceResponseDto();
        $input['uuid']=$uuid;

        //update assessmentCategory
        $updateAssessmentCategory = $this->updateAssessmentCategory($input);
        if($updateAssessmentCategory->isSuccess()){
            $assessmentElementInputs = $input['assessmentElements'];
            $assessmentElementUuids = [];
            foreach ($assessmentElementInputs as $assessmentElementInput){
                $param=[
                    'assessmentCategoryUuid'=>$uuid,
                    'uuid'=>$assessmentElementInput['uuid'],
                    'assessmentElement'=>$assessmentElementInput['assessmentElement'],
                    'elementIntegrity'=>$assessmentElementInput['elementIntegrity']
                ];
                $assessmentElementUuids[] = $assessmentElementInput['uuid'];
                $updateAssessmentElement = $this->updateAssessmentElement($param);
                if($updateAssessmentElement->isSuccess()){
                    $assessmentElementScaleInputs = $assessmentElementInput['assessmentElementScales'];
                    $assessmentElementScaleUuids = [];
                    foreach ($assessmentElementScaleInputs as $assessmentElementScaleInput){
                        $assessmentElementScaleUuids[]=$assessmentElementScaleInput['uuid'];
                        $param = [
                            'uuid'=>$assessmentElementScaleInput['uuid'],
                            'elementUuid'=>$assessmentElementInput['uuid'],
                            'textScale'=>$assessmentElementScaleInput['textScale'],
                            'valueScale'=>$assessmentElementScaleInput['valueScale']
                        ];
                        $updateAssessmentElementScale = $this->updateAssessmentElementScale($param);
                        if(!$updateAssessmentElementScale->isSuccess()){
                            $response->addErrorMessage($updateAssessmentElementScale->getErrorMessages());
                        }
                    }
                    try{
                        $this->assessmentElementScalesRepository->massDelete($assessmentElementInput['uuid'],$assessmentElementScaleUuids);
                    }catch (\Exception $exception){
                        $response->addErrorMessage($exception->getMessage());
                    }
                }else{

                    $response->addErrorMessage($updateAssessmentElement->getErrorMessages());
                }
            }
            try{
                $this->assessmentElementsRepository->massDelete($uuid,$assessmentElementUuids);
            }catch (\Exception $exception){
                $response->addErrorMessage($exception->getMessage());
            }
        }else{
            $response->addErrorMessage($updateAssessmentCategory->getErrorMessages());
        }

        return $response;
    }

    public function read($id){
        $response = new ServiceResponseDto();
        $data = [];
        $assessmentElementData = [];
        $assessmentCategory = $this->assessmentCategoriesRepository->read($id);
        $assessmentElements = $this->assessmentElementsRepository->readByAssessmentCategory($id);
        foreach($assessmentElements as $assessmentElement){
            $assessmentElementScalesData = [];
            $assessmentElementScales = $this->assessmentElementScalesRepository->getByAssessmentElement($assessmentElement['uuid']);
            foreach($assessmentElementScales as $assessmentElementScale){
                $assessmentElementScalesData[]=[
                    'uuid'=>$assessmentElementScale['uuid'],
                    'elementUuid'=>$assessmentElementScale['assessmentElementUuid'],
                    'textScale'=>$assessmentElementScale['textScale'],
                    'valueScale'=>$assessmentElementScale['valueScale']
                ];
            }
            $assessmentElementData[] =[
                'uuid'=>$assessmentElement['uuid'],
                'assessmentCategoryUuid'=>$assessmentElement['assessmentCategoryUuid'],
                'assessmentElement'=>$assessmentElement['assessmentElement'],
                'elementIntegrity'=>$assessmentElement['elementIntegrity'],
                'assessmentElementScales'=>$assessmentElementScalesData
            ];
        }

        $data =[
            'uuid'=>$assessmentCategory['uuid'],
            'assessmentCategory'=>$assessmentCategory['assessmentCategory'],
            'assessmentType'=>$assessmentCategory['assessmentType'],
            'taskUuid'=>$assessmentCategory['taskUuid'],
            'participantRank'=>$assessmentCategory['participantRank'],
            'assessmentElements'=>$assessmentElementData
        ];
        $response->setResult($data);

        return $response;
    }

    public function showAll(){
        return $this->getAllObject($this->assessmentCategoriesRepository);
    }

    public function delete($uuid){
        $response = new ServiceResponseDto();
        try{
            $elements = $this->assessmentElementsRepository->readByAssessmentCategory($uuid);
            foreach($elements as $element){
                $this->assessmentElementScalesRepository->deleteByElementUuid($element['uuid']);
            }
            $this->assessmentElementsRepository->deleteByAssessmentCategory($uuid);
            $this->assessmentCategoriesRepository->delete($uuid);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function getParticipantByGroupTask($groupUuid,$taskuuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->participantGroupsRepository->getByGroupByTask($groupUuid,$taskuuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function getForpenByTravelingAssessmentType($isTraveling,$assessmentType,$taskUuid){
        $response = new ServiceResponseDto();
        $data =[];
        try{
           $assessmentCategories = $this->assessmentCategoriesRepository->getByTravelingType($isTraveling,$assessmentType,$taskUuid);
           foreach ($assessmentCategories as $assessmentCategory){
               $elementData =[];
               $elements = $this->assessmentElementsRepository->readByAssessmentCategory($assessmentCategory['uuid']);
               foreach($elements as $element){
                   $elementScaleData = [];
                   $elementScales = $this->assessmentElementScalesRepository->getByAssessmentElement($element['uuid']);
                   foreach($elementScales as $elementScale){
                       $elementScaleData[]=[
                           'elementUuid'=>$elementScale['assessmentElementUuid'],
                           'uuid'=>$elementScale['uuid'],
                           'textScale'=>$elementScale['textScale'],
                           'valueScale'=>$elementScale['valueScale'],
                           'score'=>0,
                           'bobot'=>0,
                           'value'=>0,
                       ];
                   }
                   $elementData[]=[
                       'elementUuid'=>$element['uuid'],
                       'assessmentCategoryUuid'=>$element['assessmentCategoryUuid'],
                       'assessmentElement'=>$element['assessmentElement'],
                       'elementIntegrity'=>$element['elementIntegrity'],
                       'total'=>0,
                       'elementScales'=>$elementScaleData
                   ];
               }
               $data[]=[
                   'assessmentCategoryUuid'=>$assessmentCategory['uuid'],
                   'isTraveling'=>$assessmentCategory['isTraveling'],
                   'assessmentType'=>$assessmentCategory['assessmentType'],
                   'assessmentCategory'=>$assessmentCategory['assessmentCategory'],
                   'total'=>0,
                   'elements'=>$elementData
               ];
           }
           $response->setResult($data);
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function isAssessmentExist($assessmentType,$taskUuid,$groupUuid,$judgeId){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->assessmentRepository->isAssessmentExist($assessmentType,$taskUuid,$groupUuid,$judgeId));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getByTravelingType($isTraveling,$assessmentType,$taskUuid,$groupUuid,$judgeId){
        $response = new ServiceResponseDto();
        $isAssessmentExsit = $this->isAssessmentExist($assessmentType,$taskUuid,$groupUuid,$judgeId)->getResult();
        if($isAssessmentExsit){
            $disable = true;
        }else{
            $disable = false;
        }
        try{
            $getParticipant = $this->getParticipantByGroupTask($groupUuid,$taskUuid);
            if($getParticipant->isSuccess()){
                $participants = $getParticipant->getResult();
                foreach ($participants as $participant){
                    $getForpen = $this->getForpenByTravelingAssessmentType($isTraveling,$assessmentType,$taskUuid);
                    if($getForpen->isSuccess()){
                        $forpen = $getForpen->getResult();
                        $assessmentModel[]=[
                            'groupUuid'=>$participant['groupUuid'],
                            'participantId'=>$participant['participantId'],
                            'fullName'=>$participant['fullName'],
                            'contingentName'=>$participant['contingentName'],
                            'positionName'=>$participant['positionName'],
                            'photograph'=>$participant['photograph'],
                            'resultAssessments'=>array(),
                            'assessments'=>$forpen
                        ];
                    }else{
                        $response->addErrorMessage($getForpen->getErrorMessages());
                    }
                }
                $getForpen = $this->getForpenByTravelingAssessmentType($isTraveling,$assessmentType,$taskUuid)->getResult();
                $data['format'] = $getForpen;
                $data['assessmentParticipants']= $assessmentModel;
                $data['disable']=$disable;
                $data['isExist']=$isAssessmentExsit;
                $response->setResult($data);
            }else{
                $response->addErrorMessage($getParticipant->getErrorMessages());
            }
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }
}