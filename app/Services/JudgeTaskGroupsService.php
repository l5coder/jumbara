<?php
/**
 * Created by PhpStorm.
 * User: syaikhulhadi
 * Date: 2019-06-04
 * Time: 01:56
 */

namespace App\Services;


use App\Repositories\Actions\TasksRepository;
use App\Repositories\Contracts\IJudgeTaskGroupsRepository;
use App\Repositories\Contracts\IParticipantRepository;
use App\Repositories\Contracts\IUserRepository;
use App\Services\Response\ServiceResponseDto;

class JudgeTaskGroupsService extends BaseService
{
    protected $judgeTaskGroupsRepository;
    protected $userRepository;
    protected $participantRepository;
    protected $tasksRepository;

    public function __construct(IJudgeTaskGroupsRepository $judgeTaskGroupsRepository, IUserRepository $userRepository, IParticipantRepository $participantRepository,
                                TasksRepository $tasksRepository)
    {
        $this->judgeTaskGroupsRepository = $judgeTaskGroupsRepository;
        $this->userRepository = $userRepository;
        $this->participantRepository = $participantRepository;
        $this->tasksRepository = $tasksRepository;
    }

    protected function isJudgeExistOnUserGroup($userId, $groupUuid, $taskUuid)
    {
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->judgeTaskGroupsRepository->isJudgeExistOnUserGroup($userId, $groupUuid, $taskUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function createUserJudge($input)
    {
        $response = new ServiceResponseDto();

        try {
            $response->setResult($this->userRepository->create($input));
        } catch (\Exception $exception) {
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function updateParticipantUserId($participantId, $userId)
    {
        $response = new ServiceResponseDto();

        try {
            $this->participantRepository->updateParticipantUserId($participantId, $userId);
        } catch (\Exception $exception) {
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    protected function isUserNameExist($userName)
    {
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->userRepository->isUserNameExist($userName));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function create($input)
    {
        $response = new ServiceResponseDto();

        try{
            $participant = $this->participantRepository->read($input['participantId']);
            $userParam = [
                'userName' => $participant['participantId'] . '' . $participant['participantNumber'],
                'name' => $participant['fullName'],
                'password' => 'jurijumbara9',
                'userLevelId' => 10,
                'contingentId' => $participant['contingentId']
            ];
//            $isUserNameExist = $this->isUserNameExist($userParam['userName'])->getResult();
            if ($participant['userId']==null) {
                $createUser = $this->createUserJudge($userParam);
                if ($createUser->isSuccess()) {
                    $userId = $createUser->getResult();
                    $updateParticipantUser = $this->updateParticipantUserId($input['participantId'], $userId);
                    if ($updateParticipantUser->isSuccess()) {
                        $isJudgeExistOnUserGroup = $this->isJudgeExistOnUserGroup($userId, $input['groupUuid'], $input['taskUuid'])->getResult();
                        if (!$isJudgeExistOnUserGroup) {
                            $input['userId'] = $userId;
                            $this->judgeTaskGroupsRepository->create($input);
                        } else {
                            $response->addErrorMessage('Penilai sudah ada dalam kelompok pada bidang yang sama');
                        }
                    } else {
                        $response->addErrorMessage($updateParticipantUser->getErrorMessages());
                    }
                } else {
                    $response->addErrorMessage($createUser->getErrorMessages());
                }
            } else {
                $userId = $participant['userId'];
                $isJudgeExistOnUserGroup = $this->isJudgeExistOnUserGroup($userId, $input['groupUuid'], $input['taskUuid'])->getResult();
                if (!$isJudgeExistOnUserGroup) {
                    $input['userId'] = $userId;
                    $this->judgeTaskGroupsRepository->create($input);
                } else {
                    $response->addErrorMessage('Penilai sudah ada dalam kelompok pada bidang yang sama');
                }
            }
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function read($uuid)
    {
        return $this->readObject($this->judgeTaskGroupsRepository, $uuid);
    }

    public function showAll()
    {
        return $this->getAllObject($this->judgeTaskGroupsRepository);
    }

    public function update($input, $uuid)
    {
        $response = new ServiceResponseDto();
        $input['uuid'] = $uuid;
        try {
            $participant = $this->participantRepository->read($input['participantId']);
            if ($participant['userId'] != null) {
                $isJudgeExistOnUserGroup = $this->isJudgeExistOnUserGroup($participant['userId'], $input['groupUuid'], $input['taskUuid'])->getResult();
                if (!$isJudgeExistOnUserGroup) {
                    $input['userId'] = $participant['userId'];
                    $this->judgeTaskGroupsRepository->update($input);
                } else {
                    $response->addErrorMessage('Penilai sudah ada dalam kelompok pada bidang yang sama');
                }
            } else {
                $userParam = [
                    'userName' => $participant['participantId'] . '' . $participant['participantNumber'],
                    'password' => 'jurijumbara9',
                    'userLevelId' => 10,
                    'contingentId' => $participant['contingentId']
                ];
                $createUser = $this->createUserJudge($userParam);
                if ($createUser->isSuccess()) {
                    $userId = $createUser->getResult();
                    $updateParticipantUser = $this->updateParticipantUserId($input['participantId'], $userId);
                    if ($updateParticipantUser->isSuccess()) {
                        $isJudgeExistOnUserGroup = $this->isJudgeExistOnUserGroup($userId, $input['groupUuid'], $input['taskUuid'])->getResult();
                        if (!$isJudgeExistOnUserGroup) {
                            $input['userId'] = $userId;
                            $this->judgeTaskGroupsRepository->update($input);
                        } else {
                            $response->addErrorMessage('Penilia sudah ada dalam kelompok pada bidang yang sama');
                        }
                    } else {
                        $response->addErrorMessage($updateParticipantUser->getErrorMessages());
                    }
                } else {
                    $response->addErrorMessage($createUser->getErrorMessages());
                }
            }
        } catch (\Exception $exception) {
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function delete($uuid)
    {
        return $this->deleteObject($this->judgeTaskGroupsRepository, $uuid);
    }

    public function getJudgeTask($judgeId)
    {
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->judgeTaskGroupsRepository->getJudgeTask($judgeId));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

    public function getByJudgeTask($judgeId,$taskUuid){
        $response = new ServiceResponseDto();

        try{
            $response->setResult($this->judgeTaskGroupsRepository->getByJudgeTask($judgeId,$taskUuid));
        }catch (\Exception $exception){
            $response->addErrorMessage($exception->getMessage());
        }

        return $response;
    }

}