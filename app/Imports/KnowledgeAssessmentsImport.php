<?php

namespace App\Imports;

use App\Models\KnowledgeAssessmentsModel;
use Maatwebsite\Excel\Concerns\ToModel;

class KnowledgeAssessmentsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new KnowledgeAssessmentsModel([
            'participant_id'=>$row['0'],
            'task_uuid'=>$row[1],
            'score'=>$row[2]
        ]);
    }
}
