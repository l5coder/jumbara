<script src="{{asset('vendor/admin-assets/js/plugins/loaders/pace.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/core/libraries/jquery.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/core/libraries/bootstrap.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/loaders/blockui.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/ui/nicescroll.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/ui/drilldown.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/ui/moment/moment.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/forms/styling/switch.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/pickers/pickadate/picker.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/ui/ripple.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
<script src="{{asset('vendor/admin-assets/js/demo_pages/dashboard.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>


<script src="{{asset('js/app.js')}}"></script>
