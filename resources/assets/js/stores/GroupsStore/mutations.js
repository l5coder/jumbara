import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.taskGroups = data;
    },
    [types.READ](state,{index,data}){
        if(index == null){
            state.taskGroup = data
        }else{
            state.taskGroup = state.taskGroups[index]
        }
    },
    [types.EDIT](state,{index,data}){
        state.taskGroups[index] = data
    },
    [types.ADD](state,{data}){
        state.taskGroups.push(data)
    },
    [types.DELETE](state,{index}){
        state.taskGroups.splice(index,1)
    }
};
