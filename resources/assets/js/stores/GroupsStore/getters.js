export function getGroup(state) {
    return state.taskGroup;
}

export function getGroups(state) {
    return state.taskGroups;
}
