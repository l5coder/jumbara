import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.judgeTaskGroups = data;
    },
    [types.READ](state,{index,data}){
        if(index == null){
            state.judgeTaskGroup = data
        }else{
            state.judgeTaskGroup = state.judgeTaskGroups[index]
        }
    },
    [types.EDIT](state,{index,data}){
        state.judgeTaskGroups[index] = data
    },
    [types.ADD](state,{data}){
        state.judgeTaskGroups.push(data)
    },
    [types.DELETE](state,{index}){
        state.judgeTaskGroups.splice(index,1)
    }
};
