export function getJudgeTaskGroups(state) {
    return state.judgeTaskGroups;
}

export function getJudgeTaskGroup(state) {
    return state.judgeTaskGroup
}
