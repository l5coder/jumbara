import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, { data }) {
        state.assessments = data;
    },
    [types.READ](state,{index,data}){
        if(index == null){
            state.assessment = data
        }else{
            state.assessment = state.assessments[index]
        }
    },
    [types.EDIT](state,{index,data}){
        state.assessments[index] = data
    },
    [types.ADD](state,{data}){
        state.assessments.push(data)
    },
    [types.DELETE](state,{index}){
        state.assessments.splice(index,1)
    },
    [types.ASSESSMENT_RESULT_NON_TRAVELING](state,{data}){
        state.assessmentResultNonTraveling = data
    },
    [types.ASSESSMENT_RESULT_TRAVELING](state,{data}){
        state.assessmentResultTraveling = data
    },
    [types.ASSESSMENT_RESULT_FASILITATOR](state,{data}){
        state.assessmentResultFasilitator = data;
    }
};
