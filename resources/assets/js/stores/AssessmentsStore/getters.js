export function getAssessments(state) {
    return state.assessments;
}

export function getAssessment(state) {
    return state.assessment
}

export function getAssessmentResultNonTraveling(state) {
    return state.assessmentResultNonTraveling
}

export function getAssessmentResultTraveling(state) {
    return state.assessmentResultTraveling
}

export function getAssessmentResultFasilitatorTraveling(state) {
    return state.assessmentResultFasilitator
}
