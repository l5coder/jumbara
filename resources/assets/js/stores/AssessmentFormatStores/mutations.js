import * as types from '../mutation-types';

export default {
    [types.BROWSE](state, {data}) {
        state.assessmentFormats = data;
    },
    [types.READ](state, {index, data}) {
        if (index == null) {
            state.assessmentFormat = data
        } else {
            state.assessmentFormat = state.assessmentFormats[index]
        }
    },
    [types.EDIT](state, {assessmentParticipantIndex, assessmentCategoryUuid, assessmentElementUuid, assessmentType, scaleUuid, valueScale, bobot, score, judgingType}) {
        if (judgingType == 'Individu') {
            if (state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments.length > 0) {
                let index = state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments.findIndex(data => data.assessmentElementUuid == assessmentElementUuid)
                if (index >= 0) {
                    state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments[index].scaleUuid = scaleUuid
                    state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments[index].valueScale = valueScale
                    state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments[index].assessmentType = assessmentType
                    state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments[index].bobot = bobot
                    state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments[index].score = score
                } else {
                    let newResult = {
                        assessmentCategoryUuid: assessmentCategoryUuid,
                        assessmentElementUuid: assessmentElementUuid,
                        assessmentType: assessmentType,
                        scaleUuid: scaleUuid,
                        valueScale: valueScale,
                        bobot: bobot,
                        score: score
                    }
                    state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments.push(newResult)
                }
            } else {
                let newResult = {
                    assessmentCategoryUuid: assessmentCategoryUuid,
                    assessmentElementUuid: assessmentElementUuid,
                    assessmentType: assessmentType,
                    scaleUuid: scaleUuid,
                    valueScale: valueScale,
                    bobot: bobot,
                    score: score
                }
                state.assessmentFormats.assessmentParticipants[assessmentParticipantIndex].resultAssessments.push(newResult)
            }
        } else {
            state.assessmentFormats['assessmentParticipants'].forEach((participant) => {
                let index = participant.resultAssessments.findIndex(data => data.assessmentElementUuid == assessmentElementUuid)
                if (index >= 0) {
                    participant.resultAssessments[index].scaleUuid = scaleUuid
                    participant.resultAssessments[index].valueScale = valueScale
                    participant.resultAssessments[index].assessmentType = assessmentType
                    participant.resultAssessments[index].bobot = bobot
                    participant.resultAssessments[index].score = score
                } else {
                    let newResult = {
                        assessmentCategoryUuid: assessmentCategoryUuid,
                        assessmentElementUuid: assessmentElementUuid,
                        assessmentType: assessmentType,
                        scaleUuid: scaleUuid,
                        valueScale: valueScale,
                        bobot: bobot,
                        score: score
                    }
                    participant.resultAssessments.push(newResult)
                }
            })
        }
    },
    [types.ADD](state, {data}) {
        state.assessmentFormats.push(data)
    },
    [types.DELETE](state, {index}) {
        state.assessmentFormats.splice(index, 1)
    },
    [types.RESET](state){
        state.assessmentFormats = null
        state.assessmentFormat = null
    }
};
