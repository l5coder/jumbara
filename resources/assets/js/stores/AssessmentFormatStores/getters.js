export function getAssessmentFormat(state) {
    return state.assessmentFormat;
}

export function getAssessmentFormats(state) {
    return state.assessmentFormats;
}
