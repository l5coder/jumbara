import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './router'
import VueSweetAlert from 'vue-sweetalert2'
import Mixin from './mixin'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import '@deveodk/vue-toastr/dist/@deveodk/vue-toastr.css'
import Toasted from 'vue-toasted';
import moment from 'moment'
import VeeValidate from 'vee-validate'
import VueTable from 'vue-tables-2'
import VTooltip from 'v-tooltip'
import store from './stores/store'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'
import 'vue-select/dist/vue-select.css';
import VueHtmlToPaper from 'vue-html-to-paper';
import excel from 'vue-excel-export'

const options = {
    name: '_blank',
    specs: [
        'fullscreen=yes',
        'titlebar=yes',
        'scrollbars=yes'
    ]
}
Vue.config.productionTip = false
Vue.use(VueSweetAlert)
Vue.use(VueRouter)
Vue.use(VueAxios, Axios)
Vue.use(Toasted,{position:'top-center',fullWidth:true,duration:5000})
Vue.use(VeeValidate,{fieldsBagName:'veeFields'})
Vue.use(VueTable.ClientTable)
Vue.use(VTooltip)
Vue.use(VueHtmlToPaper,options)
Vue.use(excel)

const Routers = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: Routes,
})

Vue.router = Routers
Vue.store = store


App.router = Vue.router
App.store = Vue.store

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js')
    // notFoundRedirect:{path:'/'},
    // authRedirect:{path:'/login'}
})

Axios.defaults.baseURL = "http://jumbara9jatim.info/api"
Axios.defaults.timeout = 100000000000000


Vue.mixin(Mixin)

Vue.directive('focus',{
    inserted(el){
        el.focus()
    }
})

new Vue(App).$mount('#app');


// new Vue({
//   render: h => h(App)
// }).$mount('#app')